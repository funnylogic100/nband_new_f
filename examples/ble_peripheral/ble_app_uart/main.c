/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "app_button.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp.h"
#include "bsp_btn_ble.h"

#include "SEGGER_RTT.h"

#include "ble_dfu.h"
#include "dfu_app_handler.h"
#include "device_manager.h"
#include "pstorage.h" // pairing

#include "nrf_drv_twi.h"
#include "nrf_drv_config.h"
#include "nrf_drv_spi.h"

#include "app_pwm.h"

#define IS_SRVC_CHANGED_CHARACT_PRESENT 1                                           /**< Include the service_changed characteristic. If not enabled, the server's database cannot be changed for the lifetime of the device. */

#define DEVICE_NAME                     "NaverBAND"                               /**< Name of device. Will be included in the advertising data. */
#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      180                                         /**< The advertising timeout (in units of seconds). */

#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_MAX_TIMERS            (8 + BSP_APP_TIMERS_NUMBER)//(7 + BSP_APP_TIMERS_NUMBER)                 /**< Maximum number of simultaneously created timers. */
#define APP_TIMER_OP_QUEUE_SIZE         6                                           /**< Size of timer operation queues. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER) /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define START_STRING                    "Start...\n"                                /**< The string that will be sent over the UART when the application starts. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define UART_TX_BUF_SIZE                1024//256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256//256                                         /**< UART RX buffer size. */

static ble_nus_t                        m_nus;                                      /**< Structure to identify the Nordic UART Service. */
static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */

static ble_uuid_t                       m_adv_uuids[] = {{BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}};  /**< Universally unique service identifier. */

bool vvtest; 
int vvtestint;
static app_timer_id_t                   m_battery_timer_id;  

const unsigned char c_chFont1608[95][16] = 
{	  
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//" ",0
	{0x00,0x00,0x00,0x00,0x00,0x00,0x1F,0xCC,0x00,0x0C,0x00,0x00,0x00,0x00,0x00,0x00},//"!",1
	{0x00,0x00,0x08,0x00,0x30,0x00,0x60,0x00,0x08,0x00,0x30,0x00,0x60,0x00,0x00,0x00},//""",2
	{0x02,0x20,0x03,0xFC,0x1E,0x20,0x02,0x20,0x03,0xFC,0x1E,0x20,0x02,0x20,0x00,0x00},//"#",3
	{0x00,0x00,0x0E,0x18,0x11,0x04,0x3F,0xFF,0x10,0x84,0x0C,0x78,0x00,0x00,0x00,0x00},//"$",4
	{0x0F,0x00,0x10,0x84,0x0F,0x38,0x00,0xC0,0x07,0x78,0x18,0x84,0x00,0x78,0x00,0x00},//"%",5
	{0x00,0x78,0x0F,0x84,0x10,0xC4,0x11,0x24,0x0E,0x98,0x00,0xE4,0x00,0x84,0x00,0x08},//"&",6
	{0x08,0x00,0x68,0x00,0x70,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"'",7
	{0x00,0x00,0x00,0x00,0x00,0x00,0x07,0xE0,0x18,0x18,0x20,0x04,0x40,0x02,0x00,0x00},//"(",8
	{0x00,0x00,0x40,0x02,0x20,0x04,0x18,0x18,0x07,0xE0,0x00,0x00,0x00,0x00,0x00,0x00},//")",9
	{0x02,0x40,0x02,0x40,0x01,0x80,0x0F,0xF0,0x01,0x80,0x02,0x40,0x02,0x40,0x00,0x00},//"*",10
	{0x00,0x80,0x00,0x80,0x00,0x80,0x0F,0xF8,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x00},//"+",11
	{0x00,0x01,0x00,0x0D,0x00,0x0E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//",",12
	{0x00,0x00,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80},//"-",13
	{0x00,0x00,0x00,0x0C,0x00,0x0C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//".",14
	{0x00,0x00,0x00,0x06,0x00,0x18,0x00,0x60,0x01,0x80,0x06,0x00,0x18,0x00,0x20,0x00},//"/",15
	{0x00,0x00,0x07,0xF0,0x08,0x08,0x10,0x04,0x10,0x04,0x08,0x08,0x07,0xF0,0x00,0x00},//"0",16
	{0x00,0x00,0x08,0x04,0x08,0x04,0x1F,0xFC,0x00,0x04,0x00,0x04,0x00,0x00,0x00,0x00},//"1",17
	{0x00,0x00,0x0E,0x0C,0x10,0x14,0x10,0x24,0x10,0x44,0x11,0x84,0x0E,0x0C,0x00,0x00},//"2",18
	{0x00,0x00,0x0C,0x18,0x10,0x04,0x11,0x04,0x11,0x04,0x12,0x88,0x0C,0x70,0x00,0x00},//"3",19
	{0x00,0x00,0x00,0xE0,0x03,0x20,0x04,0x24,0x08,0x24,0x1F,0xFC,0x00,0x24,0x00,0x00},//"4",20
	{0x00,0x00,0x1F,0x98,0x10,0x84,0x11,0x04,0x11,0x04,0x10,0x88,0x10,0x70,0x00,0x00},//"5",21
	{0x00,0x00,0x07,0xF0,0x08,0x88,0x11,0x04,0x11,0x04,0x18,0x88,0x00,0x70,0x00,0x00},//"6",22
	{0x00,0x00,0x1C,0x00,0x10,0x00,0x10,0xFC,0x13,0x00,0x1C,0x00,0x10,0x00,0x00,0x00},//"7",23
	{0x00,0x00,0x0E,0x38,0x11,0x44,0x10,0x84,0x10,0x84,0x11,0x44,0x0E,0x38,0x00,0x00},//"8",24
	{0x00,0x00,0x07,0x00,0x08,0x8C,0x10,0x44,0x10,0x44,0x08,0x88,0x07,0xF0,0x00,0x00},//"9",25
	{0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x0C,0x03,0x0C,0x00,0x00,0x00,0x00,0x00,0x00},//":",26
	{0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x06,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//";",27
	{0x00,0x00,0x00,0x80,0x01,0x40,0x02,0x20,0x04,0x10,0x08,0x08,0x10,0x04,0x00,0x00},//"<",28
	{0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x00,0x00},//"=",29
	{0x00,0x00,0x10,0x04,0x08,0x08,0x04,0x10,0x02,0x20,0x01,0x40,0x00,0x80,0x00,0x00},//">",30
	{0x00,0x00,0x0E,0x00,0x12,0x00,0x10,0x0C,0x10,0x6C,0x10,0x80,0x0F,0x00,0x00,0x00},//"?",31
	{0x03,0xE0,0x0C,0x18,0x13,0xE4,0x14,0x24,0x17,0xC4,0x08,0x28,0x07,0xD0,0x00,0x00},//"@",32
	{0x00,0x04,0x00,0x3C,0x03,0xC4,0x1C,0x40,0x07,0x40,0x00,0xE4,0x00,0x1C,0x00,0x04},//"A",33
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x04,0x11,0x04,0x0E,0x88,0x00,0x70,0x00,0x00},//"B",34
	{0x03,0xE0,0x0C,0x18,0x10,0x04,0x10,0x04,0x10,0x04,0x10,0x08,0x1C,0x10,0x00,0x00},//"C",35
	{0x10,0x04,0x1F,0xFC,0x10,0x04,0x10,0x04,0x10,0x04,0x08,0x08,0x07,0xF0,0x00,0x00},//"D",36
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x04,0x17,0xC4,0x10,0x04,0x08,0x18,0x00,0x00},//"E",37
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x00,0x17,0xC0,0x10,0x00,0x08,0x00,0x00,0x00},//"F",38
	{0x03,0xE0,0x0C,0x18,0x10,0x04,0x10,0x04,0x10,0x44,0x1C,0x78,0x00,0x40,0x00,0x00},//"G",39
	{0x10,0x04,0x1F,0xFC,0x10,0x84,0x00,0x80,0x00,0x80,0x10,0x84,0x1F,0xFC,0x10,0x04},//"H",40
	{0x00,0x00,0x10,0x04,0x10,0x04,0x1F,0xFC,0x10,0x04,0x10,0x04,0x00,0x00,0x00,0x00},//"I",41
	{0x00,0x03,0x00,0x01,0x10,0x01,0x10,0x01,0x1F,0xFE,0x10,0x00,0x10,0x00,0x00,0x00},//"J",42
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x03,0x80,0x14,0x64,0x18,0x1C,0x10,0x04,0x00,0x00},//"K",43
	{0x10,0x04,0x1F,0xFC,0x10,0x04,0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x0C,0x00,0x00},//"L",44
	{0x10,0x04,0x1F,0xFC,0x1F,0x00,0x00,0xFC,0x1F,0x00,0x1F,0xFC,0x10,0x04,0x00,0x00},//"M",45
	{0x10,0x04,0x1F,0xFC,0x0C,0x04,0x03,0x00,0x00,0xE0,0x10,0x18,0x1F,0xFC,0x10,0x00},//"N",46
	{0x07,0xF0,0x08,0x08,0x10,0x04,0x10,0x04,0x10,0x04,0x08,0x08,0x07,0xF0,0x00,0x00},//"O",47
	{0x10,0x04,0x1F,0xFC,0x10,0x84,0x10,0x80,0x10,0x80,0x10,0x80,0x0F,0x00,0x00,0x00},//"P",48
	{0x07,0xF0,0x08,0x18,0x10,0x24,0x10,0x24,0x10,0x1C,0x08,0x0A,0x07,0xF2,0x00,0x00},//"Q",49
	{0x10,0x04,0x1F,0xFC,0x11,0x04,0x11,0x00,0x11,0xC0,0x11,0x30,0x0E,0x0C,0x00,0x04},//"R",50
	{0x00,0x00,0x0E,0x1C,0x11,0x04,0x10,0x84,0x10,0x84,0x10,0x44,0x1C,0x38,0x00,0x00},//"S",51
	{0x18,0x00,0x10,0x00,0x10,0x04,0x1F,0xFC,0x10,0x04,0x10,0x00,0x18,0x00,0x00,0x00},//"T",52
	{0x10,0x00,0x1F,0xF8,0x10,0x04,0x00,0x04,0x00,0x04,0x10,0x04,0x1F,0xF8,0x10,0x00},//"U",53
	{0x10,0x00,0x1E,0x00,0x11,0xE0,0x00,0x1C,0x00,0x70,0x13,0x80,0x1C,0x00,0x10,0x00},//"V",54
	{0x1F,0xC0,0x10,0x3C,0x00,0xE0,0x1F,0x00,0x00,0xE0,0x10,0x3C,0x1F,0xC0,0x00,0x00},//"W",55
	{0x10,0x04,0x18,0x0C,0x16,0x34,0x01,0xC0,0x01,0xC0,0x16,0x34,0x18,0x0C,0x10,0x04},//"X",56
	{0x10,0x00,0x1C,0x00,0x13,0x04,0x00,0xFC,0x13,0x04,0x1C,0x00,0x10,0x00,0x00,0x00},//"Y",57
	{0x08,0x04,0x10,0x1C,0x10,0x64,0x10,0x84,0x13,0x04,0x1C,0x04,0x10,0x18,0x00,0x00},//"Z",58
	{0x00,0x00,0x00,0x00,0x00,0x00,0x7F,0xFE,0x40,0x02,0x40,0x02,0x40,0x02,0x00,0x00},//"[",59
	{0x00,0x00,0x30,0x00,0x0C,0x00,0x03,0x80,0x00,0x60,0x00,0x1C,0x00,0x03,0x00,0x00},//"\",60
	{0x00,0x00,0x40,0x02,0x40,0x02,0x40,0x02,0x7F,0xFE,0x00,0x00,0x00,0x00,0x00,0x00},//"]",61
	{0x00,0x00,0x00,0x00,0x20,0x00,0x40,0x00,0x40,0x00,0x40,0x00,0x20,0x00,0x00,0x00},//"^",62
	{0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x01},//"_",63
	{0x00,0x00,0x40,0x00,0x40,0x00,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"`",64
	{0x00,0x00,0x00,0x98,0x01,0x24,0x01,0x44,0x01,0x44,0x01,0x44,0x00,0xFC,0x00,0x04},//"a",65
	{0x10,0x00,0x1F,0xFC,0x00,0x88,0x01,0x04,0x01,0x04,0x00,0x88,0x00,0x70,0x00,0x00},//"b",66
	{0x00,0x00,0x00,0x70,0x00,0x88,0x01,0x04,0x01,0x04,0x01,0x04,0x00,0x88,0x00,0x00},//"c",67
	{0x00,0x00,0x00,0x70,0x00,0x88,0x01,0x04,0x01,0x04,0x11,0x08,0x1F,0xFC,0x00,0x04},//"d",68
	{0x00,0x00,0x00,0xF8,0x01,0x44,0x01,0x44,0x01,0x44,0x01,0x44,0x00,0xC8,0x00,0x00},//"e",69
	{0x00,0x00,0x01,0x04,0x01,0x04,0x0F,0xFC,0x11,0x04,0x11,0x04,0x11,0x00,0x18,0x00},//"f",70
	{0x00,0x00,0x00,0xD6,0x01,0x29,0x01,0x29,0x01,0x29,0x01,0xC9,0x01,0x06,0x00,0x00},//"g",71
	{0x10,0x04,0x1F,0xFC,0x00,0x84,0x01,0x00,0x01,0x00,0x01,0x04,0x00,0xFC,0x00,0x04},//"h",72
	{0x00,0x00,0x01,0x04,0x19,0x04,0x19,0xFC,0x00,0x04,0x00,0x04,0x00,0x00,0x00,0x00},//"i",73
	{0x00,0x00,0x00,0x03,0x00,0x01,0x01,0x01,0x19,0x01,0x19,0xFE,0x00,0x00,0x00,0x00},//"j",74
	{0x10,0x04,0x1F,0xFC,0x00,0x24,0x00,0x40,0x01,0xB4,0x01,0x0C,0x01,0x04,0x00,0x00},//"k",75
	{0x00,0x00,0x10,0x04,0x10,0x04,0x1F,0xFC,0x00,0x04,0x00,0x04,0x00,0x00,0x00,0x00},//"l",76
	{0x01,0x04,0x01,0xFC,0x01,0x04,0x01,0x00,0x01,0xFC,0x01,0x04,0x01,0x00,0x00,0xFC},//"m",77
	{0x01,0x04,0x01,0xFC,0x00,0x84,0x01,0x00,0x01,0x00,0x01,0x04,0x00,0xFC,0x00,0x04},//"n",78
	{0x00,0x00,0x00,0xF8,0x01,0x04,0x01,0x04,0x01,0x04,0x01,0x04,0x00,0xF8,0x00,0x00},//"o",79
	{0x01,0x01,0x01,0xFF,0x00,0x85,0x01,0x04,0x01,0x04,0x00,0x88,0x00,0x70,0x00,0x00},//"p",80
	{0x00,0x00,0x00,0x70,0x00,0x88,0x01,0x04,0x01,0x04,0x01,0x05,0x01,0xFF,0x00,0x01},//"q",81
	{0x01,0x04,0x01,0x04,0x01,0xFC,0x00,0x84,0x01,0x04,0x01,0x00,0x01,0x80,0x00,0x00},//"r",82
	{0x00,0x00,0x00,0xCC,0x01,0x24,0x01,0x24,0x01,0x24,0x01,0x24,0x01,0x98,0x00,0x00},//"s",83
	{0x00,0x00,0x01,0x00,0x01,0x00,0x07,0xF8,0x01,0x04,0x01,0x04,0x00,0x00,0x00,0x00},//"t",84
	{0x01,0x00,0x01,0xF8,0x00,0x04,0x00,0x04,0x00,0x04,0x01,0x08,0x01,0xFC,0x00,0x04},//"u",85
	{0x01,0x00,0x01,0x80,0x01,0x70,0x00,0x0C,0x00,0x10,0x01,0x60,0x01,0x80,0x01,0x00},//"v",86
	{0x01,0xF0,0x01,0x0C,0x00,0x30,0x01,0xC0,0x00,0x30,0x01,0x0C,0x01,0xF0,0x01,0x00},//"w",87
	{0x00,0x00,0x01,0x04,0x01,0x8C,0x00,0x74,0x01,0x70,0x01,0x8C,0x01,0x04,0x00,0x00},//"x",88
	{0x01,0x01,0x01,0x81,0x01,0x71,0x00,0x0E,0x00,0x18,0x01,0x60,0x01,0x80,0x01,0x00},//"y",89
	{0x00,0x00,0x01,0x84,0x01,0x0C,0x01,0x34,0x01,0x44,0x01,0x84,0x01,0x0C,0x00,0x00},//"z",90
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x3E,0xFC,0x40,0x02,0x40,0x02},//"{",91
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00},//"|",92
	{0x00,0x00,0x40,0x02,0x40,0x02,0x3E,0xFC,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//"}",93
	{0x00,0x00,0x60,0x00,0x80,0x00,0x80,0x00,0x40,0x00,0x40,0x00,0x20,0x00,0x20,0x00},//"~",94
};

const unsigned char c_chFont1206[95][12] = 
{
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*" ",0*/
	{0x00,0x00,0x00,0x00,0x3F,0x40,0x00,0x00,0x00,0x00,0x00,0x00},/*"!",1*/
	{0x00,0x00,0x30,0x00,0x40,0x00,0x30,0x00,0x40,0x00,0x00,0x00},/*""",2*/
	{0x09,0x00,0x0B,0xC0,0x3D,0x00,0x0B,0xC0,0x3D,0x00,0x09,0x00},/*"#",3*/
	{0x18,0xC0,0x24,0x40,0x7F,0xE0,0x22,0x40,0x31,0x80,0x00,0x00},/*"$",4*/
	{0x18,0x00,0x24,0xC0,0x1B,0x00,0x0D,0x80,0x32,0x40,0x01,0x80},/*"%",5*/
	{0x03,0x80,0x1C,0x40,0x27,0x40,0x1C,0x80,0x07,0x40,0x00,0x40},/*"&",6*/
	{0x10,0x00,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*"'",7*/
	{0x00,0x00,0x00,0x00,0x00,0x00,0x1F,0x80,0x20,0x40,0x40,0x20},/*"(",8*/
	{0x00,0x00,0x40,0x20,0x20,0x40,0x1F,0x80,0x00,0x00,0x00,0x00},/*")",9*/
	{0x09,0x00,0x06,0x00,0x1F,0x80,0x06,0x00,0x09,0x00,0x00,0x00},/*"*",10*/
	{0x04,0x00,0x04,0x00,0x3F,0x80,0x04,0x00,0x04,0x00,0x00,0x00},/*"+",11*/
	{0x00,0x10,0x00,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*",",12*/
	{0x04,0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x00,0x00},/*"-",13*/
	{0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*".",14*/
	{0x00,0x20,0x01,0xC0,0x06,0x00,0x38,0x00,0x40,0x00,0x00,0x00},/*"/",15*/
	{0x1F,0x80,0x20,0x40,0x20,0x40,0x20,0x40,0x1F,0x80,0x00,0x00},/*"0",16*/
	{0x00,0x00,0x10,0x40,0x3F,0xC0,0x00,0x40,0x00,0x00,0x00,0x00},/*"1",17*/
	{0x18,0xC0,0x21,0x40,0x22,0x40,0x24,0x40,0x18,0x40,0x00,0x00},/*"2",18*/
	{0x10,0x80,0x20,0x40,0x24,0x40,0x24,0x40,0x1B,0x80,0x00,0x00},/*"3",19*/
	{0x02,0x00,0x0D,0x00,0x11,0x00,0x3F,0xC0,0x01,0x40,0x00,0x00},/*"4",20*/
	{0x3C,0x80,0x24,0x40,0x24,0x40,0x24,0x40,0x23,0x80,0x00,0x00},/*"5",21*/
	{0x1F,0x80,0x24,0x40,0x24,0x40,0x34,0x40,0x03,0x80,0x00,0x00},/*"6",22*/
	{0x30,0x00,0x20,0x00,0x27,0xC0,0x38,0x00,0x20,0x00,0x00,0x00},/*"7",23*/
	{0x1B,0x80,0x24,0x40,0x24,0x40,0x24,0x40,0x1B,0x80,0x00,0x00},/*"8",24*/
	{0x1C,0x00,0x22,0xC0,0x22,0x40,0x22,0x40,0x1F,0x80,0x00,0x00},/*"9",25*/
	{0x00,0x00,0x00,0x00,0x08,0x40,0x00,0x00,0x00,0x00,0x00,0x00},/*":",26*/
	{0x00,0x00,0x00,0x00,0x04,0x60,0x00,0x00,0x00,0x00,0x00,0x00},/*";",27*/
	{0x00,0x00,0x04,0x00,0x0A,0x00,0x11,0x00,0x20,0x80,0x40,0x40},/*"<",28*/
	{0x09,0x00,0x09,0x00,0x09,0x00,0x09,0x00,0x09,0x00,0x00,0x00},/*"=",29*/
	{0x00,0x00,0x40,0x40,0x20,0x80,0x11,0x00,0x0A,0x00,0x04,0x00},/*">",30*/
	{0x18,0x00,0x20,0x00,0x23,0x40,0x24,0x00,0x18,0x00,0x00,0x00},/*"?",31*/
	{0x1F,0x80,0x20,0x40,0x27,0x40,0x29,0x40,0x1F,0x40,0x00,0x00},/*"@",32*/
	{0x00,0x40,0x07,0xC0,0x39,0x00,0x0F,0x00,0x01,0xC0,0x00,0x40},/*"A",33*/
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x24,0x40,0x1B,0x80,0x00,0x00},/*"B",34*/
	{0x1F,0x80,0x20,0x40,0x20,0x40,0x20,0x40,0x30,0x80,0x00,0x00},/*"C",35*/
	{0x20,0x40,0x3F,0xC0,0x20,0x40,0x20,0x40,0x1F,0x80,0x00,0x00},/*"D",36*/
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x2E,0x40,0x30,0xC0,0x00,0x00},/*"E",37*/
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x2E,0x00,0x30,0x00,0x00,0x00},/*"F",38*/
	{0x0F,0x00,0x10,0x80,0x20,0x40,0x22,0x40,0x33,0x80,0x02,0x00},/*"G",39*/
	{0x20,0x40,0x3F,0xC0,0x04,0x00,0x04,0x00,0x3F,0xC0,0x20,0x40},/*"H",40*/
	{0x20,0x40,0x20,0x40,0x3F,0xC0,0x20,0x40,0x20,0x40,0x00,0x00},/*"I",41*/
	{0x00,0x60,0x20,0x20,0x20,0x20,0x3F,0xC0,0x20,0x00,0x20,0x00},/*"J",42*/
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x0B,0x00,0x30,0xC0,0x20,0x40},/*"K",43*/
	{0x20,0x40,0x3F,0xC0,0x20,0x40,0x00,0x40,0x00,0x40,0x00,0xC0},/*"L",44*/
	{0x3F,0xC0,0x3C,0x00,0x03,0xC0,0x3C,0x00,0x3F,0xC0,0x00,0x00},/*"M",45*/
	{0x20,0x40,0x3F,0xC0,0x0C,0x40,0x23,0x00,0x3F,0xC0,0x20,0x00},/*"N",46*/
	{0x1F,0x80,0x20,0x40,0x20,0x40,0x20,0x40,0x1F,0x80,0x00,0x00},/*"O",47*/
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x24,0x00,0x18,0x00,0x00,0x00},/*"P",48*/
	{0x1F,0x80,0x21,0x40,0x21,0x40,0x20,0xE0,0x1F,0xA0,0x00,0x00},/*"Q",49*/
	{0x20,0x40,0x3F,0xC0,0x24,0x40,0x26,0x00,0x19,0xC0,0x00,0x40},/*"R",50*/
	{0x18,0xC0,0x24,0x40,0x24,0x40,0x22,0x40,0x31,0x80,0x00,0x00},/*"S",51*/
	{0x30,0x00,0x20,0x40,0x3F,0xC0,0x20,0x40,0x30,0x00,0x00,0x00},/*"T",52*/
	{0x20,0x00,0x3F,0x80,0x00,0x40,0x00,0x40,0x3F,0x80,0x20,0x00},/*"U",53*/
	{0x20,0x00,0x3E,0x00,0x01,0xC0,0x07,0x00,0x38,0x00,0x20,0x00},/*"V",54*/
	{0x38,0x00,0x07,0xC0,0x3C,0x00,0x07,0xC0,0x38,0x00,0x00,0x00},/*"W",55*/
	{0x20,0x40,0x39,0xC0,0x06,0x00,0x39,0xC0,0x20,0x40,0x00,0x00},/*"X",56*/
	{0x20,0x00,0x38,0x40,0x07,0xC0,0x38,0x40,0x20,0x00,0x00,0x00},/*"Y",57*/
	{0x30,0x40,0x21,0xC0,0x26,0x40,0x38,0x40,0x20,0xC0,0x00,0x00},/*"Z",58*/
	{0x00,0x00,0x00,0x00,0x7F,0xE0,0x40,0x20,0x40,0x20,0x00,0x00},/*"[",59*/
	{0x00,0x00,0x70,0x00,0x0C,0x00,0x03,0x80,0x00,0x40,0x00,0x00},/*"\",60*/
	{0x00,0x00,0x40,0x20,0x40,0x20,0x7F,0xE0,0x00,0x00,0x00,0x00},/*"]",61*/
	{0x00,0x00,0x20,0x00,0x40,0x00,0x20,0x00,0x00,0x00,0x00,0x00},/*"^",62*/
	{0x00,0x10,0x00,0x10,0x00,0x10,0x00,0x10,0x00,0x10,0x00,0x10},/*"_",63*/
	{0x00,0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*"`",64*/
	{0x00,0x00,0x02,0x80,0x05,0x40,0x05,0x40,0x03,0xC0,0x00,0x40},/*"a",65*/
	{0x20,0x00,0x3F,0xC0,0x04,0x40,0x04,0x40,0x03,0x80,0x00,0x00},/*"b",66*/
	{0x00,0x00,0x03,0x80,0x04,0x40,0x04,0x40,0x06,0x40,0x00,0x00},/*"c",67*/
	{0x00,0x00,0x03,0x80,0x04,0x40,0x24,0x40,0x3F,0xC0,0x00,0x40},/*"d",68*/
	{0x00,0x00,0x03,0x80,0x05,0x40,0x05,0x40,0x03,0x40,0x00,0x00},/*"e",69*/
	{0x00,0x00,0x04,0x40,0x1F,0xC0,0x24,0x40,0x24,0x40,0x20,0x00},/*"f",70*/
	{0x00,0x00,0x02,0xE0,0x05,0x50,0x05,0x50,0x06,0x50,0x04,0x20},/*"g",71*/
	{0x20,0x40,0x3F,0xC0,0x04,0x40,0x04,0x00,0x03,0xC0,0x00,0x40},/*"h",72*/
	{0x00,0x00,0x04,0x40,0x27,0xC0,0x00,0x40,0x00,0x00,0x00,0x00},/*"i",73*/
	{0x00,0x10,0x00,0x10,0x04,0x10,0x27,0xE0,0x00,0x00,0x00,0x00},/*"j",74*/
	{0x20,0x40,0x3F,0xC0,0x01,0x40,0x07,0x00,0x04,0xC0,0x04,0x40},/*"k",75*/
	{0x20,0x40,0x20,0x40,0x3F,0xC0,0x00,0x40,0x00,0x40,0x00,0x00},/*"l",76*/
	{0x07,0xC0,0x04,0x00,0x07,0xC0,0x04,0x00,0x03,0xC0,0x00,0x00},/*"m",77*/
	{0x04,0x40,0x07,0xC0,0x04,0x40,0x04,0x00,0x03,0xC0,0x00,0x40},/*"n",78*/
	{0x00,0x00,0x03,0x80,0x04,0x40,0x04,0x40,0x03,0x80,0x00,0x00},/*"o",79*/
	{0x04,0x10,0x07,0xF0,0x04,0x50,0x04,0x40,0x03,0x80,0x00,0x00},/*"p",80*/
	{0x00,0x00,0x03,0x80,0x04,0x40,0x04,0x50,0x07,0xF0,0x00,0x10},/*"q",81*/
	{0x04,0x40,0x07,0xC0,0x02,0x40,0x04,0x00,0x04,0x00,0x00,0x00},/*"r",82*/
	{0x00,0x00,0x06,0x40,0x05,0x40,0x05,0x40,0x04,0xC0,0x00,0x00},/*"s",83*/
	{0x00,0x00,0x04,0x00,0x1F,0x80,0x04,0x40,0x00,0x40,0x00,0x00},/*"t",84*/
	{0x04,0x00,0x07,0x80,0x00,0x40,0x04,0x40,0x07,0xC0,0x00,0x40},/*"u",85*/
	{0x04,0x00,0x07,0x00,0x04,0xC0,0x01,0x80,0x06,0x00,0x04,0x00},/*"v",86*/
	{0x06,0x00,0x01,0xC0,0x07,0x00,0x01,0xC0,0x06,0x00,0x00,0x00},/*"w",87*/
	{0x04,0x40,0x06,0xC0,0x01,0x00,0x06,0xC0,0x04,0x40,0x00,0x00},/*"x",88*/
	{0x04,0x10,0x07,0x10,0x04,0xE0,0x01,0x80,0x06,0x00,0x04,0x00},/*"y",89*/
	{0x00,0x00,0x04,0x40,0x05,0xC0,0x06,0x40,0x04,0x40,0x00,0x00},/*"z",90*/
	{0x00,0x00,0x00,0x00,0x04,0x00,0x7B,0xE0,0x40,0x20,0x00,0x00},/*"{",91*/
	{0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xF0,0x00,0x00,0x00,0x00},/*"|",92*/
	{0x00,0x00,0x40,0x20,0x7B,0xE0,0x04,0x00,0x00,0x00,0x00,0x00},/*"}",93*/
	{0x40,0x00,0x80,0x00,0x40,0x00,0x20,0x00,0x20,0x00,0x40,0x00},/*"~",94*/
}; 

const unsigned char c_chFont0608[11][8] = 
{
	{0x73,0x8b,0x8b,0x8b,0x8b,0x8b,0x8b,0x73},/*"0",0*/
	{0x23,0xe3,0x23,0x23,0x23,0x23,0x23,0x23},/*"1",1*/
	{0x73,0x8b,0x8b,0x13,0x23,0x43,0x83,0xfb},/*"2",2*/
	{0x73,0x8b,0x0b,0x73,0x0b,0x0b,0x8b,0x73},/*"3",3*/
	{0x33,0x33,0x53,0x53,0x93,0xfb,0x13,0x13},/*"4",4*/
	{0xfb,0x83,0x83,0xf3,0x0b,0x0b,0x8b,0x73},/*"5",5*/
	{0x73,0x8b,0x83,0xf3,0x8b,0x8b,0x8b,0x73},/*"6",6*/
	{0xfb,0x0b,0x13,0x13,0x13,0x23,0x23,0x23},/*"7",7*/
	{0x73,0x8b,0x8b,0x73,0x8b,0x8b,0x8b,0x73},/*"8",8*/
	{0x73,0x8b,0x8b,0x8b,0x7b,0x0b,0x8b,0x73},/*"9",9*/
	{0x03,0x03,0x03,0x03,0x03,0x03,0x33,0x33},/*":",10*/
};

//////////////////////
// 공용
//////////////////////
typedef    unsigned long     uint32;
typedef    unsigned int      uint16;
typedef    unsigned char     uint8;

#define DELAY_MS_100             100
#define DELAY_MS                 10//1000

static void deviceInit();

#define    TRUE     1
#define    FALSE    0

//////////////////////
// Data type
//////////////////////
#define MAX_QUEUE_SIZE 1000

typedef struct {
    //char sBuffer[256];
    char queue[MAX_QUEUE_SIZE+1];
    char sBufferTemp[40];
	char sBufferForHeader[4];
    uint8 readLength;
    int remainLength;
    uint8 index;
    uint16 ucid;
    bool more;
    uint8 iLoop;
    int front, rear;
    bool sending;
} DATA_SEND_T;

DATA_SEND_T dataSendBuffer;

typedef struct {
	uint8 flashPageChecker;
	uint8 flashPageToRead;

	uint16 pageNum;
	uint16 sectorNum;
} FLASH_MAP_T;

FLASH_MAP_T flashMap;

typedef struct {
    uint8 time_year;
	uint8 time_month;
	uint8 time_day;
	uint8 time_hh;
	uint8 time_mm;
	uint8 time_ss;
	uint32 data_count;
	uint16 timeTotalBackup;
	uint16 timeTotal;
} DATA_TIMESTAMP_T;

DATA_TIMESTAMP_T dataTimeStamp;

#define LE8_L(x)                       ((x) & 0xff)
#define LE8_XOR(x)                     ((x) ^ 0xff)

// ddd
bool OLED_SHOWING = false;
bool IS_CALL_NUM_SCROLLING;  // 전화번호 스크롤중인가?


//////////////////////
// Test
//////////////////////
bool showshow = false;
bool showshowPre = false;
int showshowCount = 0;

bool FIRST_INIT = false;

//////////////////////
// 펌웨어 버전가져오기
//////////////////////
bool GET_FIRM_VER = false; // 펌웨어 버전 가져오기 커맨드
#define FIRM_REV_MAJOR 0x00       
#define FIRM_REV_MINOR 0x01 
#define FIRM_REVISION  ((FIRM_REV_MAJOR << 8) | FIRM_REV_MINOR)  

static void notiToAppFirmwareVersion();
//////////////////////
// Advertising
//////////////////////
#define APP_ADV_FAST_INTERVAL           40                                                  /**< The advertising interval (in units of 0.625 ms. This value corresponds to 25 ms). */
#define APP_ADV_SLOW_INTERVAL           3200                                                /**< Slow advertising interval (in units of 0.625 ms. This value corresponds to 2 seconds). */
#define APP_ADV_FAST_TIMEOUT            30                                                  /**< The duration of the fast advertising period (in seconds). */
#define APP_ADV_SLOW_TIMEOUT            180                                                 /**< The advertising timeout in units of seconds. */

//////////////////////
// PWM
//////////////////////
bool STOP_VIBE;
bool ING_PWM;
bool IS_PWM_ENABLE;
bool PWM_ON;
unsigned char vibePattern[15];
bool vibeNew = false;
uint8 vibePatternIndex = 0;
bool isVibeOn = false;
unsigned char vibeData;

uint8 stopVibeCount;

uint8 motorStatusChangeCount;
uint8 vibePatternCount = 0;

#define MOTOR_A_PWM_PIN     23//8//22
#define MOTOR_A_EN_PIN      24//9//23
#define MOTOR_A_LAR         22

APP_PWM_INSTANCE(PWM1,1);                   // Create the instance "PWM1" using TIMER1.
void pwm_ready_callback(uint32_t pwm_id);
static volatile bool ready_flag;            // A flag indicating PWM status.

void pwm_start_A(void);
void pwm_stop_A(void);

//////////////////////
// OLED
//////////////////////
#define MIN_OLED 0
#define PAGE_TOTAL 4
#define START_PAGE 0xB0
#define START_HIGH_BIT 0x12
#define START_LOW_BIT 0x00
#define COLUMN_MAX 64
#define ROW_MAX 32

void Clear_Screen(unsigned char chFill);
void Clear_Screen_test(unsigned char chFill);

static uint32_t _dc, _rs, _cs;

#define _HI(p)      nrf_gpio_pin_set(p)
#define _LO(p)      nrf_gpio_pin_clear(p)

#define _HI_RS()    _HI(_rs)
#define _LO_RS()    _LO(_rs)
#define _HI_DC()    _HI(_dc)
#define _LO_DC()    _LO(_dc)
#define _HI_CS()    _HI(_cs)
#define _LO_CS()    _LO(_cs)

#define SSD1306_CONFIG_VDD_PIN      2
#define SSD1306_CONFIG_CLK_PIN      28
#define SSD1306_CONFIG_MOSI_PIN     29
#define SSD1306_CONFIG_CS_PIN       25
#define SSD1306_CONFIG_DC_PIN       27 // miso
#define SSD1306_CONFIG_RST_PIN      26

static void oledPinInit(uint32_t dc, uint32_t rs, uint32_t cs, uint32_t clk, uint32_t mosi);
static void oledICReset();
static void oledInit();
static void oledWriteCommand(unsigned char chData);
static void oledWriteData(unsigned char date);
void spi_send_recv_oled(uint8_t * const p_tx_data,
                   uint8_t * const p_rx_data,
                   const uint16_t len);
uint8_t* spi_transfer_oled(uint8_t * message, const uint16_t len);
				   
#define SSD1306_WIDTH    64
#define SSD1306_HEIGHT   32
//static unsigned char s_chDispalyBuffer[OLED_W][OLED_H]; // [128][8]
static unsigned char s_chDispalyBuffer[64][8]; // [128][8]	
	
void ssd1306_refresh_gram(void);				   
void ssd1306_clear_screen(unsigned char chFill);
void ssd1306_clear_data(unsigned char chFill);
				   
unsigned char c_chFont1608_app[10][16] = 
{	  
//	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//" ",0
	{0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA},// 화면에 픽셀이 찍히는지 테스트하기 위한 용도
};			

void ssd1306_draw_point(unsigned char chXpos, unsigned char chYpos, unsigned char chPoint);
void ssd1306_draw_0608char(unsigned char chXpos, unsigned char chYpos, unsigned char chChar);
void ssd1306_display_string(unsigned char chXpos, unsigned char  chYpos, const unsigned char  *pchString, unsigned char  chSize, unsigned char  chMode);
void ssd1306_display_char(unsigned char chXpos, unsigned char chYpos, unsigned char chChr, unsigned char chSize, unsigned char chMode);
	
//////////////////////
// Command
//////////////////////
bool CMD_MEMORY_CLEAR; // flash memory clear command

//////////////////////
// Motion Sensor
//////////////////////
#define BMI160_ADDR                    0x68
#define MOTION_SENSOR_ADDR             BMI160_ADDR   

#define BMI160_CMD_COMMANDS_ADDR				(0X7E)
#define BMI160_REG_ACCEL_RANGE   				(0X41)  // Register ACC_RANGE
#define BMI160_USER_DATA_14_ADDR				(0X12) // ACCEL_X (MSB)
#define BMI160_ACCEL_DATA_LENGTH	 (2)
#define BMI160_ACCEL_XYZ_DATA_LENGTH (6)


//////////////////////
// 활동관련
//////////////////////
bool REAL_STEP = false;
bool EVT_STEP = false;
bool stepCheck;   // step count 모드

//////////////////////
// Clock
//////////////////////
bool SHOW_CURRENT_TIME; // 현재 시간을 보여주는 준다.
unsigned char strCurruntTime[5];

static void clockInit();
static void updateTime(uint8 gap);
static void updateTimeForTest(uint8 gap);

//////////////////////
// Noti
//////////////////////
static void noti_timeout_handler(void * p_context);
static void notiFlashEraseComplete();

//////////////////////
// Statue
//////////////////////
bool passHandler; // 모션센서에서 값을 읽을것인가?
bool SAVE_DATA;   // flash memory에 모션센서 값을 기록
static bool readRun;

static void sensorModeChange(uint8 mode);

//////////////////////
// Flash
//////////////////////
#define SENSOR_DATA_SIZE 6
static const nrf_drv_spi_t m_spi_master_memory = NRF_DRV_SPI_INSTANCE(1);
static const nrf_drv_spi_t m_spi_master_oled   = NRF_DRV_SPI_INSTANCE(2);

static void spi_master_memory_event_handler(nrf_drv_spi_event_t event);
static void spi_master_oled_event_handler(nrf_drv_spi_event_t event);

static volatile bool m_transfer_completed_memory = true; /**< A flag to inform about completed transfer. */
static volatile bool m_transfer_completed_oled = true; /**< A flag to inform about completed transfer. */
	
static void spiInit();

#define TX_BUF_SIZE 40
static uint8_t m_tx_data[TX_BUF_SIZE] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; /**< A buffer with data to transfer. */
static uint8_t m_rx_data[TX_BUF_SIZE] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; /**< A buffer for incoming data. */
static uint8_t row_data[20] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; /**< A buffer for incoming data. */

static uint32 flash_addr_r;  // raw data read start address
static uint32 flash_addr;    // raw data write start address

#define ACTIVITY_BASE       0x000000 
#define ACTIVITY_LIMIT_SIZE 0xFFFF
#define ACTIVITY_COUNT      (ACTIVITY_LIMIT_SIZE + 1) // 2byte
static uint32  flash_addr_activity;
static uint32  flash_addr_activity_r;

static void spi_send_recv(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len, bool rw_mode);
						  
#define RW_MODE_R 0
#define RW_MODE_W 1				

#define FLASH_TARGET_ADDR 0x020000   // 2번 블럭부터는 raw 데이터를 저장하자
//#define FLASH_TARGET_ADDR 0x7FF000 // memory full test 용
#define FLASH_TARGET_SIZE 0x7FFFFF		

static uint8_t rcvBuffer[SENSOR_DATA_SIZE];	
static void enqueue(char* data, uint16 len);
static void dequeue();						  

static int is_full();
static int is_empty();

uint16 remainCount = 0;
bool isSaveData;	

static void notiFlashReadLast();
static void notiFlashReadFull();
static void notiFlashWriteFull();

static void getChecksum(char* value, uint8 size, uint16 *total);
static void savedDataSend(void); // Flash 메모리에 저장된 활동정보를 BT를 통해 앱으로 전송
static void savedDataClear(void);
//////////////////////
// Timer
//////////////////////
#define MS_BETWEEN_SAMPLING_ADC		10000		      /* e.g.: fire timeout every 10 seconds */
#define MAIN_TIMER_INTERVAL         APP_TIMER_TICKS(200, APP_TIMER_PRESCALER)
#define TIMER_TICKS		            APP_TIMER_TICKS(MS_BETWEEN_SAMPLING_ADC, APP_TIMER_PRESCALER)
static app_timer_id_t main_timer_id;
static app_timer_id_t save_timer_id;
static app_timer_id_t noti_timer_id;
static void main_timeout_handler(void * p_context);
static void save_timeout_handler(void * p_context);


//////////////////////
// Motion Sensor
//////////////////////
#define BMI160_ADDR                    0x68
#define MOTION_SENSOR_ADDR             BMI160_ADDR   

#define BMI160_CMD_COMMANDS_ADDR				(0X7E)
#define BMI160_USER_DATA_14_ADDR				(0X12) // ACCEL_X (MSB)
#define BMI160_ACCEL_DATA_LENGTH	 (2)
#define BMI160_ACCEL_XYZ_DATA_LENGTH (6)

#define MOTION_SENSOR_SCL         7  
#define MOTION_SENSOR_SDA         6  

#define IN_LINE_PRINT_CNT        6  //<! Number of data bytes printed in single line

#define MASTER_TWI_INST          0    //!< TWI interface used as a master accessing EEPROM memory
#if( TWI0_ENABLED + TWI1_ENABLED ) > 0
static const nrf_drv_twi_t m_twi_master = NRF_DRV_TWI_INSTANCE(MASTER_TWI_INST);
#endif

static uint32_t motion_sensor_read(size_t addr, uint8_t * pdata, size_t size);
static uint32_t motion_sensor_read_acc_xyz(size_t addr, uint8_t * pdata, size_t size);
static uint32_t motion_sensor_write(size_t addr, uint8_t * pdata, size_t size);
static ret_code_t twi_master_init(void);

signed short acc_x, acc_y, acc_z = 0; // 읽어온 가속센서값

uint32_t main_timer_err_code;
size_t sensor_addr = 0x12;
uint8_t sensor_read_buff[IN_LINE_PRINT_CNT]; 

//////////////////////
// DFU
//////////////////////
#define DFU_REV_MAJOR                    0x00                                       /** DFU Major revision number to be exposed. */
#define DFU_REV_MINOR                    0x04                                       /** DFU Minor revision number to be exposed. */
#define DFU_REVISION                     ((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)     /** DFU Revision number to be exposed. Combined of major and minor versions. */

#define SEC_PARAM_BOND                  1                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE//BLE_GAP_IO_CAPS_DISPLAY_ONLY //BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                           /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                          /**< Maximum encryption key size. */


static dm_application_instance_t m_app_handle;                               /**< Application identifier allocated by device manager. */
static ble_gap_sec_params_t      m_sec_param;       

ble_dfu_init_t dfus_init;
static ble_dfu_t m_dfus;  
static void reset_prepare();
static void advertising_stop(void);
static void device_manager_init(bool erase_bonds);
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t const  * p_event,
                                           ret_code_t        event_result);										   										  

/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse 
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of 
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_nus    Nordic UART Service structure.
 * @param[in] p_data   Data to be send to UART module.
 * @param[in] length   Length of the data.
 */
/**@snippet [Handling the data received over BLE] */
static void nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data, uint16_t length)
{
uint16 err_code;
	//printf("nus_data_handler\n");
	
	/*
    for (uint32_t i = 0; i < length; i++)
    {
		printf("data is %x\n", p_data[i]);
        while(app_uart_put(p_data[i]) != NRF_SUCCESS);
    }
    while(app_uart_put('\n') != NRF_SUCCESS);
	*/
	LEDS_ON(BSP_LED_3_MASK);
	err_code = app_timer_start(noti_timer_id, MAIN_TIMER_INTERVAL, NULL);
	SEGGER_RTT_printf(0, "start : %d\n", err_code);
	
	SEGGER_RTT_printf(0, "packet, %x, %x, %x\n", p_data[0], p_data[1], p_data[2]);
	
	nrf_delay_ms(DELAY_MS);
	
	if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0x12 ) && ( p_data[2] == 0x01 ) ) { // 가속센서 모드 변경
        if( p_data[3] == 0x00 ) {  // 스텝카운트 측정
            sensorModeChange(0);
        }
        else if( p_data[3] == 0x01 ) { // 실시간 raw data 측정
            sensorModeChange(1);			
        }
        else if( p_data[3] == 0x02 ) { // 수면 측정
            sensorModeChange(2);
        }
        else if( p_data[3] == 0x03 ) { // 실시간 raw data 메모리에 저장
            sensorModeChange(3);
        }
		else if( p_data[3] == 0x4 ) { // 실시간 걸음수
            sensorModeChange(4);
        }
    }  
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xC9 ) && ( p_data[2] == 0x01 ) ) { // 모션센서 주기 변경
        if( p_data[3] == 0x01 ) {  // 7.81Hz
//            Bml055SetBW( BMI055_ACC_BW_1 );
        }
        else if( p_data[3] == 0x02 ) { // 15.63Hz
        //    Bml055SetBW( BMI055_ACC_BW_2 );
        }
        else if( p_data[3] == 0x03 ) { // 31.25Hz
        //    Bml055SetBW( BMI055_ACC_BW_3 );
        }
        else if( p_data[3] == 0x04 ) { // 62.5Hz
         //   Bml055SetBW( BMI055_ACC_BW_4 );
        }
        else if( p_data[3] == 0x05 ) { // 125Hz
         //   Bml055SetBW( BMI055_ACC_BW_5 );
        }
        else if( p_data[3] == 0x06 ) { // 250Hz
         //   Bml055SetBW( BMI055_ACC_BW_6 );
        }
        else if( p_data[3] == 0x07 ) { // 500Hz
         //   Bml055SetBW( BMI055_ACC_BW_7 );
        }
        else if( p_data[3] == 0x08 ) { // 1000Hz
         //   Bml055SetBW( BMI055_ACC_BW_8 );
        }
    } 
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xC8 ) && ( p_data[2] == 0x0 ) ) { // Data Symc 테스트용
        //printf("cmd : call \r\n ");
        //DataSync();
        savedDataSend();
    }
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xCB ) && ( p_data[2] == 0x0 ) ) { // 저장된 데이터수 요청
        //printf("cmd : saved data count \r\n ");
        //DataSync();
     //   savedDataCountSend();
    }
    else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0x17 ) && ( p_data[2] == 0x0 ) ) { // 데이터 초기화
        //printf("cmd : data clear \r\n ");       
		CMD_MEMORY_CLEAR = true;
    }
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xC ) && ( p_data[2] == 0x6 ) ) { // 시간설정
		SEGGER_RTT_WriteString(0, "cmd : set time \r\n ");
		
		dataTimeStamp.time_hh = p_data[6]; // 시
		dataTimeStamp.time_mm = p_data[7]; // 분
		dataTimeStamp.time_ss = p_data[8]; // 초
    } 
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xCF ) && ( p_data[2] == 0x1 ) ) { // 진동(프리셋)
		SEGGER_RTT_WriteString(0, "vibe(preset)\n");	
		
		ING_PWM = true;
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xD3 ) && ( p_data[2] == 0x10 ) ) { // vibe custom
		printf("vibe custom\n");	
		// xxx = p_data[3]; // 전체 repeat 수
		vibePattern[0]  = p_data[4];
		vibePattern[1]  = p_data[5];
		vibePattern[2]  = p_data[6];
		vibePattern[3]  = p_data[7];
		vibePattern[4]  = p_data[8];
		vibePattern[5]  = p_data[9];
		vibePattern[6]  = p_data[10];
		vibePattern[7]  = p_data[11];
		vibePattern[8]  = p_data[12];
		vibePattern[9]  = p_data[13];
		vibePattern[10] = p_data[14];
		vibePattern[11] = p_data[15];
		vibePattern[12] = p_data[16];
		vibePattern[13] = p_data[17];
		vibePattern[14] = p_data[18];
		
		vibeNew = true;
	}
	else if( ( p_data[0] == 0xFF ) && ( p_data[1] == 0xDA ) && ( p_data[2] == 0x0 ) ) { // 펌웨어 버전 가져오기
		printf("get firmware version\n");
		
		GET_FIRM_VER = true;
	}
}
/**@snippet [Handling the data received over BLE] */


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t       err_code;
    ble_nus_init_t nus_init;
    
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;
    
    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
	
	SEGGER_RTT_WriteString(0, "services_init 22\n");   
	
	// DFU
	// Initialize the Device Firmware Update Service.
    memset(&dfus_init, 0, sizeof(dfus_init));

    dfus_init.evt_handler   = dfu_app_on_dfu_evt;
    dfus_init.error_handler = NULL;
    dfus_init.evt_handler   = dfu_app_on_dfu_evt;
    dfus_init.revision      = DFU_REVISION;

	SEGGER_RTT_WriteString(0, "services_init 33\n");   
    err_code = ble_dfu_init(&m_dfus, &dfus_init);
    APP_ERROR_CHECK(err_code);
	SEGGER_RTT_WriteString(0, "services_init 44\n");   

    dfu_app_reset_prepare_set(reset_prepare);
	SEGGER_RTT_WriteString(0, "services_init 55\n");  
    dfu_app_dm_appl_instance_set(m_app_handle);
	SEGGER_RTT_WriteString(0, "services_init 66\n");  
}


/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;
    
    if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    
    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;
    
    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
			SEGGER_RTT_WriteString(0, "on_adv_evt, fast adv\n");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(err_code);
            break;
		case BLE_ADV_EVT_SLOW:
			SEGGER_RTT_WriteString(0, "on_adv_evt, slow adv\n");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING_SLOW);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_ADV_EVT_IDLE:
            //sleep_mode_enter();
			SEGGER_RTT_WriteString(0, "on_adv_evt, idle adv\n");
		
			ble_advertising_start(BLE_ADV_MODE_SLOW);
            break;
        default:
            break;
    }
}


/**@brief Function for the Application's S110 SoftDevice event handler.
 *
 * @param[in] p_ble_evt S110 SoftDevice event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t                         err_code;
    
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
            err_code = bsp_indication_set(BSP_INDICATE_IDLE);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for dispatching a S110 SoftDevice event to all modules with a S110 SoftDevice 
 *        event handler.
 *
 * @details This function is called from the S110 SoftDevice event interrupt handler after a S110 
 *          SoftDevice event has been received.
 *
 * @param[in] p_ble_evt  S110 SoftDevice event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_nus_on_ble_evt(&m_nus, p_ble_evt);
	ble_dfu_on_ble_evt(&m_dfus, p_ble_evt); // for dfu
    on_ble_evt(p_ble_evt);
    ble_advertising_on_ble_evt(p_ble_evt);
    bsp_btn_ble_on_ble_evt(p_ble_evt);    
}


/**@brief Function for the S110 SoftDevice initialization.
 *
 * @details This function initializes the S110 SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;
    
    // Initialize SoftDevice.
    //SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, NULL);

	SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_RC_250_PPM_4000MS_CALIBRATION, NULL);
	
    // Enable BLE stack.
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));
#if (defined(S130) || defined(S132))
    ble_enable_params.gatts_enable_params.attr_tab_size   = BLE_GATTS_ATTR_TAB_SIZE_DEFAULT;
#endif
    ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;
    err_code = sd_ble_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    // Subscribe for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    uint32_t err_code;
    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break;

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BSP_EVENT_WHITELIST_OFF:
			/*
            err_code = ble_advertising_restart_without_whitelist();
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
		*/
            break;

        default:
            break;
    }
}


/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to 
 *          a string. The string will be be sent over BLE when the last character received was a 
 *          'new line' i.e '\n' (hex 0x0D) or if the string has reached a length of 
 *          @ref NUS_MAX_DATA_LENGTH.
 */
/**@snippet [Handling the data received over UART] */
void uart_event_handle(app_uart_evt_t * p_event)
{/*
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
    uint32_t       err_code;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
            index++;

            if ((data_array[index - 1] == '\n') || (index >= (BLE_NUS_MAX_DATA_LEN)))
            {
                err_code = ble_nus_string_send(&m_nus, data_array, index);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
                
                index = 0;
            }
            break;

        case APP_UART_COMMUNICATION_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
	*/
}
/**@snippet [Handling the data received over UART] */


/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
static void uart_init(void)
{
	/*
    uint32_t                     err_code;
    const app_uart_comm_params_t comm_params =
    {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_ENABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud38400
    };

    APP_UART_FIFO_INIT( &comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOW,
                       err_code);
    APP_ERROR_CHECK(err_code);
	*/
}
/**@snippet [UART Initialization] */


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t scanrsp;

    // Build advertising data struct to pass into @ref ble_advertising_init.
    memset(&advdata, 0, sizeof(advdata));
    advdata.name_type          = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance = false;
    advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;

    memset(&scanrsp, 0, sizeof(scanrsp));
    scanrsp.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    scanrsp.uuids_complete.p_uuids  = m_adv_uuids;

    ble_adv_modes_config_t options = {0};
	// TODO 아래 코드를 살려야하는가? 검토해라.
	// options.ble_adv_whitelist_enabled = BLE_ADV_WHITELIST_ENABLED; // pairing 
	
	// funnylogic. advertising 시간 설정
    options.ble_adv_fast_enabled  = BLE_ADV_FAST_ENABLED;
    options.ble_adv_fast_interval = APP_ADV_FAST_INTERVAL;
    options.ble_adv_fast_timeout  = APP_ADV_FAST_TIMEOUT;
    options.ble_adv_slow_enabled  = BLE_ADV_SLOW_ENABLED;
    options.ble_adv_slow_interval = APP_ADV_SLOW_INTERVAL;
    options.ble_adv_slow_timeout  = APP_ADV_SLOW_TIMEOUT;

    err_code = ble_advertising_init(&advdata, &scanrsp, &options, on_adv_evt, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{
	
    bsp_event_t startup_event;

    uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS,
                                 APP_TIMER_TICKS(100, APP_TIMER_PRESCALER), 
                                 bsp_event_handler);
	
	SEGGER_RTT_WriteString(0, "Hello World!\n");
	SEGGER_RTT_printf(0, "bsp_init %d\n", err_code);
	//SEGGER_RTT_printf
    //APP_ERROR_CHECK(err_code);
///*
    err_code = bsp_btn_ble_init(NULL, &startup_event);
    //APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
	//*/
}


/**@brief Function for placing the application in low power state while waiting for events.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

//void app_uart_put_string(const char* s)
//{
	/*
    uint8_t len = strlen(s);
    for (uint8_t i = 0; i < len; i++)
    {
    while (app_uart_put(s[i]) != NRF_SUCCESS);
        app_uart_put(s[i]);
        //APP_ERROR_CHECK(err_code);
    }
	*/
//}

static void battery_level_meas_timeout_handler(void * p_context)
{
    uint32_t err_code;
    UNUSED_PARAMETER(p_context);
    //err_code = nrf_drv_saadc_sample();
    APP_ERROR_CHECK(err_code);
}

static void timers_init(void)
{
    uint32_t err_code;

    // Initialize timer module.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, false);

    // Create battery timer.
    err_code = app_timer_create(&m_battery_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                battery_level_meas_timeout_handler);
    APP_ERROR_CHECK(err_code);
}

/**@brief Application main function.
 */
int main(void)
{
    uint32_t err_code;
    bool erase_bonds;
	uint8_t iLoop = 0;
    //uint8_t  start_string[] = START_STRING;
	
	///////////////////////
	// TEST
	showshow = false;
	showshowCount = 0;
	showshowPre = false;
	//
	///////////////////////
	
	FIRST_INIT = true;
		
	deviceInit();
    
    // Initialize.
    //APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, false);
    //uart_init();
	//app_trace_init();
	timers_init();
	
	device_manager_init(erase_bonds);
	buttons_leds_init(&erase_bonds);
    
	SEGGER_RTT_WriteString(0, "buttons_leds_init 11\n");
    ble_stack_init();
	SEGGER_RTT_WriteString(0, "ble_stack_init 11\n");    
	
	
	
	gap_params_init();
	SEGGER_RTT_WriteString(0, "gap_params_init 11\n");    	
    services_init();
	SEGGER_RTT_WriteString(0, "services_init 11\n");    	
	
    advertising_init();
    conn_params_init();
	
	
	SEGGER_RTT_WriteString(0, "conn_params_init 11\n");
    
   //printf("%s",start_string);

    err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
    //APP_ERROR_CHECK(err_code);
	if( err_code != NRF_SUCCESS ) {
		SEGGER_RTT_printf(0, "*** ble_advertising_start : %d\n", err_code);
	}
	
#if( TWI0_ENABLED + TWI1_ENABLED ) > 0
	twi_master_init();
#endif	

	spiInit();
	
	// PMU mode for acc
	sensor_read_buff[0] = 0x11;	
	motion_sensor_write(BMI160_CMD_COMMANDS_ADDR, sensor_read_buff, 1);
	
	// Motion sensor accel range setting
	// 0x3 : +-2g range
	// 0x5 : +-4g range
	// 0x8 : +-8g range
	// 0xC : +-16g range
	sensor_read_buff[0] = 0xC;	
	motion_sensor_write(BMI160_REG_ACCEL_RANGE, sensor_read_buff, 1);
	
	// 플래시 초반데이터를 삭제한다. 그때 주소는 0x000000으로 하고 
	// 플래시를 지우고 나서는 메모리맵대로 설정하자.
	flash_addr = ACTIVITY_BASE;
	
	SEGGER_RTT_WriteString(0, "memoray erase start\n");   
	// Block Erase(64K)
	// 64M 전체 블럭을 삭제한다.
	/*
	for( iLoop = 0; iLoop < 2; iLoop++) {	 //
		m_tx_data[0] = 0x06;
		//m_transfer_completed = false;
		spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
		//while (m_transfer_completed == false);	
		nrf_delay_ms(DELAY_MS);
		
		m_tx_data[0] = 0xD8;		
		m_tx_data[1] = flash_addr >> 16;
		m_tx_data[2] = flash_addr >> 8;
		m_tx_data[3] = flash_addr;	
		
		printf("block add : %d, %x, %x, %x\n", iLoop, m_tx_data[1], m_tx_data[2], m_tx_data[3]);
		spi_send_recv(m_tx_data, m_rx_data, 4, RW_MODE_W ); // TX_RX_BUF_LENGTH		
		
		flash_addr += 0x10000;
		nrf_delay_ms(DELAY_MS_100);
	}	
	*/
	SEGGER_RTT_WriteString(0, "memoray erase end\n");  
	
	//notiFlashEraseComplete();
	
	/*
	flash_addr = FLASH_TARGET_ADDR;
	flash_addr_r = FLASH_TARGET_ADDR;
	flash_addr_activity = ACTIVITY_BASE;
	flash_addr_activity_r = ACTIVITY_BASE;
	*/
		
	//sensorModeChange( 0 ); // 네이버 글자후 스텝카운트를 보여주기 위함
	
	
 /* */
	vvtest = false;
	vvtestint = 0;
	
	//nrf_gpio_pin_dir_set(8, NRF_GPIO_PIN_DIR_OUTPUT);
	//nrf_gpio_pin_dir_set(9, NRF_GPIO_PIN_DIR_OUTPUT);
	
	size_t addr = 0;
	uint8_t buff[IN_LINE_PRINT_CNT]; 
    // Enter main loop.
	
	memset(&buff, 0, sizeof(buff));	
	
	// Create timers.
    err_code = app_timer_create(&main_timer_id,
                                APP_TIMER_MODE_REPEATED, //APP_TIMER_MODE_REPEATED,
                                main_timeout_handler);
								
	err_code = app_timer_create(&save_timer_id,
                                APP_TIMER_MODE_REPEATED, //APP_TIMER_MODE_SINGLE_SHOT,
                                save_timeout_handler);					

	err_code = app_timer_create(&noti_timer_id,
                                APP_TIMER_MODE_REPEATED, //APP_TIMER_MODE_SINGLE_SHOT,
                                noti_timeout_handler);	
								
	err_code = app_timer_start(main_timer_id, MAIN_TIMER_INTERVAL, NULL);		
	err_code = app_timer_start(save_timer_id, MAIN_TIMER_INTERVAL, NULL);
	err_code = app_timer_start(noti_timer_id, TIMER_TICKS, NULL);
	
	SEGGER_RTT_WriteString(0, "OTA 2\n");  
	
	
	//err_code = sd_power_dcdc_mode_set( NRF_POWER_DCDC_ENABLE );
	/*
	oledInit(SSD1306_CONFIG_DC_PIN,
             SSD1306_CONFIG_RST_PIN,
             SSD1306_CONFIG_CS_PIN,
             SSD1306_CONFIG_CLK_PIN,
             SSD1306_CONFIG_MOSI_PIN);
			 */
    // ccc
	
	SHOW_CURRENT_TIME = true;
	
    for (;;)
    {
		if(  FIRST_INIT == true ) {
			 FIRST_INIT = false;
						
			for( iLoop = 0; iLoop < 2; iLoop++) {	 //
				m_tx_data[0] = 0x06;
				//m_transfer_completed = false;
				spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
				//while (m_transfer_completed == false);	
				nrf_delay_ms(DELAY_MS);
				
				m_tx_data[0] = 0xD8;		
				m_tx_data[1] = flash_addr >> 16;
				m_tx_data[2] = flash_addr >> 8;
				m_tx_data[3] = flash_addr;	
				
				SEGGER_RTT_printf(0, "block add : %d, %x, %x, %x\n", iLoop, m_tx_data[1], m_tx_data[2], m_tx_data[3]);
				spi_send_recv(m_tx_data, m_rx_data, 4, RW_MODE_W ); // TX_RX_BUF_LENGTH		
				
				flash_addr += 0x10000;
				nrf_delay_ms(DELAY_MS_100);
			}	
			
			flash_addr = FLASH_TARGET_ADDR;
			flash_addr_r = FLASH_TARGET_ADDR;
			flash_addr_activity = ACTIVITY_BASE;
			flash_addr_activity_r = ACTIVITY_BASE;
			
			oledPinInit(SSD1306_CONFIG_DC_PIN,
						SSD1306_CONFIG_RST_PIN,
						SSD1306_CONFIG_CS_PIN,
						SSD1306_CONFIG_CLK_PIN,
						SSD1306_CONFIG_MOSI_PIN);	
			
			Clear_Screen(0x0);
			//Clear_Screen_test(0xFF);
			
			//ssd1306_clear_screen(0xFF);
			
			/*
			// 테스트 코드, 글자 출력 좌표확인용
			ssd1306_draw_0608char(78,54,'0');
			ssd1306_draw_0608char(10,10,'1');
			ssd1306_draw_0608char(20,20,'2');
			ssd1306_draw_0608char(30,30,'3');                               
			ssd1306_draw_0608char(40,40,'4');
			ssd1306_draw_0608char(50,50,'5');
			ssd1306_draw_0608char(50,60,'6');
			ssd1306_draw_0608char(0,50,'7');
			ssd1306_refresh_gram();
			*/
			
			/*
			// 테스트 코드, 문자열 출력 확인용
			ssd1306_display_string(0, 50, "1:2", 16, 1);
			//ssd1306_display_string(80, 15, "11:34", 16, 1);
			ssd1306_refresh_gram();	
			*/
			
			///////////////////////////////////////
			// PWM 설정
			///////////////////////////////////////
			nrf_gpio_pin_dir_set(MOTOR_A_EN_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
			nrf_gpio_pin_clear(MOTOR_A_EN_PIN);
			
			nrf_gpio_pin_dir_set(MOTOR_A_LAR, NRF_GPIO_PIN_DIR_OUTPUT);
			//nrf_gpio_pin_clear(MOTOR_A_LAR);
			nrf_gpio_pin_set(MOTOR_A_LAR);  // lar은 enable과 동일하게 제어하자. 
	
			nrf_gpio_pin_dir_set(MOTOR_A_PWM_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
			nrf_gpio_pin_clear(MOTOR_A_PWM_PIN);
			app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(50, MOTOR_A_PWM_PIN);
			
			pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;	
			err_code = app_pwm_init(&PWM1, &pwm1_cfg,pwm_ready_callback);
			
			//pwm_start_A();
		}
		
		//app_uart_put_string("kimjongdoo");
        //power_manage();
		
		/* PIO 테스트
		vvtestint++;
		
		if( (vvtestint >= 0 ) && (vvtestint <= 3000) ) {
			vvtest = true;		
		}
		else if( (vvtestint > 3000 ) && (vvtestint < 6000) ) {
			vvtest = false;
		}
		else {
			vvtestint = 0;
		}
		
		if(vvtest) {
			nrf_gpio_pin_set(8);
			nrf_gpio_pin_set(9);						
		}
		else {
			//nrf_gpio_pin_set(8);
			//nrf_gpio_pin_set(9);
			nrf_gpio_pin_clear(8);
			nrf_gpio_pin_clear(9);
		}
		*/
		
		if( SAVE_DATA == true ) {
			SAVE_DATA = false;
			
			//dataSendBuffer.sending = false;
			/*
			printf("*** save data *** %u \n", flashMap.flashPageToRead);			
			for( iLoop = 0; iLoop < 4; iLoop++ ) {
				printf("*** sBuffer : %c, %c, %c, %c, %c, %c, %c, %c, %c, %c\n", dataSendBuffer.sBufferTemp[iLoop * 10], dataSendBuffer.sBufferTemp[iLoop * 10 + 1] 
					, dataSendBuffer.sBufferTemp[iLoop * 10 + 2], dataSendBuffer.sBufferTemp[iLoop * 10 + 3], dataSendBuffer.sBufferTemp[iLoop * 10 + 4]
					, dataSendBuffer.sBufferTemp[iLoop * 10 + 5], dataSendBuffer.sBufferTemp[iLoop * 10 + 6], dataSendBuffer.sBufferTemp[iLoop * 10 + 7]
					, dataSendBuffer.sBufferTemp[iLoop * 10 + 8], dataSendBuffer.sBufferTemp[iLoop * 10 + 9]);         
			*/
			
			// Write Enable
			m_tx_data[0] = 0x06;   // write enable cmd
			spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
								
			///*
			dataSendBuffer.sBufferTemp[0] = 0x02;	 // page program cmd
			dataSendBuffer.sBufferTemp[1] = flash_addr >> 16;
			dataSendBuffer.sBufferTemp[2] = flash_addr >> 8;
			dataSendBuffer.sBufferTemp[3] = flash_addr;		
			//*/
			
			/*
			dataSendBuffer.sBufferForHeader[0] = 0x02;
			dataSendBuffer.sBufferForHeader[1] = flash_addr >> 16;
			dataSendBuffer.sBufferForHeader[2] = flash_addr >> 8;
			dataSendBuffer.sBufferForHeader[3] = flash_addr;
*/

			//printf("write size : %u\n", writeDataCount);
			SEGGER_RTT_printf(0, "Waddr: %u, %u, %u \n", dataSendBuffer.sBufferTemp[1], dataSendBuffer.sBufferTemp[2], dataSendBuffer.sBufferTemp[3]);
			//printf("write addr is : %u, %u, %u \n", dataSendBuffer.sBufferForHeader[1], dataSendBuffer.sBufferForHeader[2], dataSendBuffer.sBufferForHeader[3]);
			
			if( flashMap.flashPageToRead == 4) {
//				printf("write 4 data : %x, %x, %x, %x \n", dataSendBuffer.sBufferTemp[4], dataSendBuffer.sBufferTemp[5], dataSendBuffer.sBufferTemp[6]
//							, dataSendBuffer.sBufferTemp[7]);
			}
			
			//spi_send_recv(dataSendBuffer.sBufferForHeader, m_rx_data, 4, RW_MODE_W);
			spi_send_recv(dataSendBuffer.sBufferTemp, m_rx_data, flashMap.flashPageToRead + 4, RW_MODE_W);
			
			/*
			printf("write1 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x \n", dataSendBuffer.sBufferTemp[0], dataSendBuffer.sBufferTemp[1], 
											 dataSendBuffer.sBufferTemp[2], dataSendBuffer.sBufferTemp[3], dataSendBuffer.sBufferTemp[4], 
											 dataSendBuffer.sBufferTemp[5], dataSendBuffer.sBufferTemp[6], dataSendBuffer.sBufferTemp[7],
											 dataSendBuffer.sBufferTemp[8], dataSendBuffer.sBufferTemp[9]);
			*/
			
			/*printf("write2 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x \n", readSensorBuffer2[10], readSensorBuffer2[11], 
											 readSensorBuffer2[12], readSensorBuffer2[13], readSensorBuffer2[14], 
											 readSensorBuffer2[15], readSensorBuffer2[16], readSensorBuffer2[17],
											 readSensorBuffer2[18], readSensorBuffer2[19]);
			printf("write3 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x \n", readSensorBuffer2[20], readSensorBuffer2[21], 
											 readSensorBuffer2[22], readSensorBuffer2[23], readSensorBuffer2[24], 
											 readSensorBuffer2[25], readSensorBuffer2[26], readSensorBuffer2[27],
											 readSensorBuffer2[28], readSensorBuffer2[29]);											 
			printf("write4 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x \n", readSensorBuffer2[30], readSensorBuffer2[31], 
											 readSensorBuffer2[32], readSensorBuffer2[33], readSensorBuffer2[34], 
											 readSensorBuffer2[35], readSensorBuffer2[36], readSensorBuffer2[37],
											 readSensorBuffer2[38], readSensorBuffer2[39]);	
											*/
			
			flash_addr += flashMap.flashPageToRead;
//			writeCount++;		
			
			dataSendBuffer.sBufferTemp[0] = 0x02;	
			dataSendBuffer.sBufferTemp[1] = flash_addr >> 16;
			dataSendBuffer.sBufferTemp[2] = flash_addr >> 8;
			dataSendBuffer.sBufferTemp[3] = flash_addr;		
			//printf("write addr ed : %u, %u, %u \n", dataSendBuffer.sBufferTemp[1], dataSendBuffer.sBufferTemp[2], dataSendBuffer.sBufferTemp[3]);
			
			SEGGER_RTT_printf(0,"buffer 1 is write, %u \n", flashMap.flashPageToRead);
			
			isSaveData = false;
			
			/*
			if( writeCount >= 20) {
				passHandler = true;
			}
			*/
		}
		
		if( readRun == true ) {
			
			if( (flash_addr_r + 18) > FLASH_TARGET_SIZE ) {
				printf("+full, stop reading\n");
				
				// 메모리가 풀이면 더이상 기록하지 않는다.
				passHandler = true;  // 더 이상 모션센서를 기록하지 않는다.
				readRun = false;     // 메모리에서 값을 읽어서 보내지 않는다.
				
				notiFlashReadFull(); // 메모리를 끝까지 다 읽었음을 앱에 알린다.
			}
			else if( (flash_addr_r + 50) > flash_addr ) {			
				sensorModeChange( 0 );
				notiFlashReadLast();
			}
			else {
				//			readRun = false;
	//			printf("writeCount is 30 reached.\n");
				
				// Read
				m_tx_data[0] = 0x03;	
				m_tx_data[1] = flash_addr_r >> 16;
				m_tx_data[2] = flash_addr_r >> 8;
				m_tx_data[3] = flash_addr_r;		       
	//			m_tx_data[4] = 0;					
				
				printf("Raddr: %u, %u, %u \n", m_tx_data[1], m_tx_data[2], m_tx_data[3]);
				
				// 40바이트를 초기화한다. 
				memset(m_rx_data, 0, 40);
				memset(row_data, 0, 20);
				spi_send_recv(m_tx_data, m_rx_data, 4 + 18, RW_MODE_R);
		
				memcpy(&row_data[2], &m_rx_data[4], 18);
				flash_addr_r += 18;//SENSOR_BUF_SIZE;
				
				/*
				sendDataBuffer[0] = 1;
				sendDataBuffer[1] = 2;
				sendDataBuffer[2] = 3;
				sendDataBuffer[3] = 4;
				sendDataBuffer[4] = 5;
				sendDataBuffer[5] = 6;
				*/
				row_data[0] = 0xFF;
				row_data[1] = 0x11;
				
				///*
				printf("bt1 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x\n", row_data[0], row_data[1], row_data[2], row_data[3], 
					row_data[4], row_data[5], row_data[6], row_data[7], row_data[8], row_data[9]);
				printf("bt2 : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x\n", row_data[10], row_data[11], row_data[12], row_data[13], 
					row_data[14], row_data[15], row_data[16], row_data[17], row_data[18], row_data[19]);	
					//*/

				//err_code = ble_nus_string_send(&m_nus, m_rx_data, 20);
				err_code = ble_nus_string_send(&m_nus, row_data, 20);
				printf("send: %u\n", err_code);
			}
		}
		
		if( CMD_MEMORY_CLEAR == true ) {
			CMD_MEMORY_CLEAR = false;
			
			readRun = false;     // 읽기 중단
			passHandler = true;  // 저장 중단  
			stepCheck = false;   // 스텝 체크 중단
			
			savedDataClear();
		}
		/*
		// FOR TEST
		// 액정 켜지는걸 보이위한 소스다. 일정시간마다 전체켜기, 전체끄기를 수행한다.
		if( showshow != showshowPre ) {
			showshowPre = showshow;
			
			if( showshow == true ) {
				SEGGER_RTT_WriteString(0, "show set\n");
				Clear_Screen(0xFF);
			}
			else {
				SEGGER_RTT_WriteString(0, "show clear\n");
				Clear_Screen(0x0);
			}SEGGER_RTT_WriteString(0, "Hello World!\n");
		}
		*/
		
		if( SHOW_CURRENT_TIME == true) {
			SHOW_CURRENT_TIME = false;	
			SEGGER_RTT_WriteString(0, "##show time\n");						
			SEGGER_RTT_printf(0, "show current : %s\n", strCurruntTime);
			
			if( IS_CALL_NUM_SCROLLING == false ) {				
				//ssd1306_clear_screen(0x00);
				ssd1306_clear_data(0x00);
				ssd1306_display_string(15, 38, strCurruntTime, 16, 1);
				//ssd1306_display_string(80, 15, "11:34", 16, 1);
				ssd1306_refresh_gram();	
				
				//ssd1306_display_string(80, 15, "10:10", 16, 1);
				//ssd1306_refresh_gram( DIS_MULTI_BYTE );	
			}
			else {
				printf("show current time pass. num scrolling\n");
			}
		}
		
		if( IS_PWM_ENABLE ) {
			SEGGER_RTT_WriteString(0, "motor duty - first\n");
			IS_PWM_ENABLE = false;
			
			
			if( PWM_ON ) {
				ready_flag = false;
				SEGGER_RTT_WriteString(0, "enable motor\n");
				//printf("motor duty - first 1 \n");
				
				/*
				// Set the duty cycle - keep trying until PWM is ready..
				while (app_pwm_channel_duty_set(&PWM1, 0, 15) == NRF_ERROR_BUSY);
				
				printf("motor duty - first 2 \n");
				
				// ... or wait for callback.
				while(!ready_flag);
				
				printf("motor duty - first 3 \n");
				
				app_pwm_channel_duty_set(&PWM1, 1, 15);
				
				*/
				
				pwm_start_A();
				
				//printf("motor duty 100\n");
			}
			else {
				/*
				printf("motor duty - first 4 \n");
				ready_flag = false;
				// Set the duty cycle - keep trying until PWM is ready..
				while (app_pwm_channel_duty_set(&PWM1, 0, 0) == NRF_ERROR_BUSY);
				
				printf("motor duty - first 5 \n");
				
				// ... or wait for callback.
				while(!ready_flag);
				
				printf("motor duty - first 6 \n");
				
				app_pwm_channel_duty_set(&PWM1, 1, 0);
				*/
				
				pwm_stop_A();
				
				SEGGER_RTT_WriteString(0, "stop motor\n");
			}
		}
		
		// 펌웨어 버전가져오기 커맨드
		if( GET_FIRM_VER == true ) {
			GET_FIRM_VER = false;
			
			notiToAppFirmwareVersion();
		}
		
    } // vvv
}


/** 
 * @}
 */

//////////////////////
// DFU
//////////////////////
static void reset_prepare(void)
{
    uint32_t err_code;
    if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        // Disconnect from peer.
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
        err_code = bsp_indication_set(BSP_INDICATE_IDLE);
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        // If not connected, the device will be advertising. Hence stop the advertising.
        advertising_stop();
    }
    err_code = ble_conn_params_stop();
    APP_ERROR_CHECK(err_code);
    nrf_delay_ms(500);
}


static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t const  * p_event,
                                           ret_code_t        event_result)
{
    uint32_t err_code;
    //APP_ERROR_CHECK(event_result);
	
	printf("device_manager_evt_handler\n");
	
	if( event_result != NRF_SUCCESS ) {
		printf("device_manager_evt_handler, %d\n", event_result);
	}
	
    //ble_ancs_c_on_device_manager_evt(&m_ancs_c, p_handle, p_evt);
/*
	if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
        app_context_load(p_handle);
    }
*/	
    switch (p_event->event_id)
    {
        case DM_EVT_CONNECTION:
            //m_peer_handle = (*p_handle);
            //err_code      = app_timer_start(m_sec_req_timer_id, SECURITY_REQUEST_DELAY, NULL);
            //APP_ERROR_CHECK(err_code);
            break;

        case DM_EVT_LINK_SECURED:
            //err_code = ble_db_discovery_start(&m_ble_db_discovery,
            //                                  p_evt->event_param.p_gap_param->conn_handle);
            //APP_ERROR_CHECK(err_code);
            break; 

        default:
            break;

    }
    return NRF_SUCCESS;
}

/*
주의 
이 작업을 위해서는 device_manager.h를 추가해야하며, 해당 프로젝트의 config폴더에
다른 프로젝트에서 device_manager_cnfg.h를 복사해서 넣어야한다.
*/
static void device_manager_init(bool erase_bonds) 
{
   uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = erase_bonds};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    memcpy(&m_sec_param, &register_param.sec_param, sizeof(ble_gap_sec_params_t));

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}

static void advertising_stop(void)
{
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);
}

static uint32_t motion_sensor_read(size_t addr, uint8_t * pdata, size_t size)
{
	uint32_t ret;

	//printf("eeprom read \n");
    do
    {
       uint8_t addr8 = (uint8_t)addr;
       ret = nrf_drv_twi_tx(&m_twi_master, BMI160_ADDR, &addr8, 1, true);
       if(NRF_SUCCESS != ret)
       {
           break;
       }
       ret = nrf_drv_twi_rx(&m_twi_master, BMI160_ADDR, pdata, size, false);
    }while(0);
	
    return ret;
}

static uint32_t motion_sensor_read_acc_xyz(size_t addr, uint8_t * pdata, size_t size)
{
	uint32_t ret;

	//printf("eeprom read \n");
    do
    {
       uint8_t addr8 = (uint8_t)addr;
       ret = nrf_drv_twi_tx(&m_twi_master, BMI160_ADDR, &addr8, 1, true);
       if(NRF_SUCCESS != ret)
       {
           break;
       }
       ret = nrf_drv_twi_rx(&m_twi_master, BMI160_ADDR, pdata, size, false);
    }while(0);
	
    return ret;
}

static uint32_t motion_sensor_write(size_t addr, uint8_t * pdata, size_t size)
{
	uint32_t ret;

	//printf("eeprom read \n");
    do
    {
       uint8_t addr8 = (uint8_t)addr;
       ret = nrf_drv_twi_tx(&m_twi_master, BMI160_ADDR, &addr8, 1, true);
       if(NRF_SUCCESS != ret)
       {
           break;
       }
       ret = nrf_drv_twi_tx(&m_twi_master, BMI160_ADDR, pdata, size, false);
    }while(0);
	
    return ret;
}

#if( TWI0_ENABLED + TWI1_ENABLED ) > 0
static ret_code_t twi_master_init(void)
{
	
    ret_code_t ret;
	
    const nrf_drv_twi_config_t config =
    {
       .scl                = MOTION_SENSOR_SCL,
       .sda                = MOTION_SENSOR_SDA,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };	

	SEGGER_RTT_WriteString(0, "twi_master_init start\n");
    do
    {
        ret = nrf_drv_twi_init(&m_twi_master, &config, NULL);
        if(NRF_SUCCESS != ret)
        {
			SEGGER_RTT_printf(0, "twi_master_init fail %d\n", ret);
            break;
        }
        nrf_drv_twi_enable(&m_twi_master);
    }while(0);
	
	SEGGER_RTT_printf(0, "twi_master_init end %d\n", ret);
    return ret;
}
#endif

/*
static uint32_t motion_sensor_read(size_t addr, uint8_t * pdata, size_t size)
{
	uint32_t ret;

	//printf("eeprom read \n");
    do
    {
       uint8_t addr8 = (uint8_t)addr;
       ret = nrf_drv_twi_tx(&m_twi_master, BMI160_ADDR, &addr8, 1, true);
       if(NRF_SUCCESS != ret)
       {
           break;
       }
       ret = nrf_drv_twi_rx(&m_twi_master, BMI160_ADDR, pdata, size, false);
    }while(0);
	
    return ret;
}*/

static void main_timeout_handler(void * p_context) {	
	/* 모션센서 ID 읽어오기 
	main_timer_err_code = motion_sensor_read(0, sensor_read_buff, IN_LINE_PRINT_CNT);
	SEGGER_RTT_printf(0, "read id : %d\n", sensor_read_buff[0]);
	*/
	
	// eeprom_write(addr, (uint8_t const *)str+addr, to_write);
	//sensor_read_buff[0] = 0x11;	
	//motion_sensor_write(BMI160_CMD_COMMANDS_ADDR, sensor_read_buff, 1);
	
	/*
	main_timer_err_code = motion_sensor_read(18, sensor_read_buff, IN_LINE_PRINT_CNT);	

	SEGGER_RTT_printf(0, "read : %x, %x, %x\n", sensor_read_buff[0], sensor_read_buff[1], sensor_read_buff[2]);	
	
	acc_x = ((((signed short)sensor_read_buff[1]) << 8) | (sensor_read_buff[0]) & 0xF0);
	acc_y = ((((signed short)sensor_read_buff[3]) << 8) | (sensor_read_buff[2]) & 0xF0);
	acc_z = ((((signed short)sensor_read_buff[5]) << 8) | (sensor_read_buff[4]) & 0xF0);
	
	acc_x = acc_x >> 4;
	acc_y = acc_y >> 4;
	acc_z = acc_z >> 4;
	
	SEGGER_RTT_printf(0, "read : %x, %x, %x\n", acc_x, acc_y, acc_z);
	*/
	
	main_timer_err_code = motion_sensor_read_acc_xyz(BMI160_USER_DATA_14_ADDR, sensor_read_buff, 6);
//	SEGGER_RTT_printf(0, "read : %x, %x, %x, %x, %x, %x\n", sensor_read_buff[0], sensor_read_buff[1], sensor_read_buff[2],
//						sensor_read_buff[3], sensor_read_buff[4], sensor_read_buff[5]);	
						
	acc_x = ((((signed short)sensor_read_buff[1]) << 8) | (sensor_read_buff[0]));
	acc_y = ((((signed short)sensor_read_buff[3]) << 8) | (sensor_read_buff[2]));
	acc_z = ((((signed short)sensor_read_buff[5]) << 8) | (sensor_read_buff[4]));
	
	//SEGGER_RTT_printf(0, "read 11 : %d, %d, %d\n", acc_x, acc_y, acc_z);	
	
	remainCount = 0;
	// 데이터를 메모리에 저장하지 않는다.
	if( passHandler == true) {
	}
	else { // 데이터 저장이다. 
		/*
		for(iLoop = 0; iLoop < 6; iLoop++) {
			rcvBuffer[iLoop] = (uint8_t)('a' + iLoop);
		}
		*/
		
		memcpy(&rcvBuffer, &sensor_read_buff, 6);
		
//			printf("\n\nen : %x, %x, %x, %x, %x, %x\n", rcvBuffer[0], rcvBuffer[1], rcvBuffer[2], rcvBuffer[3], rcvBuffer[4], rcvBuffer[5]);
		
		enqueue( rcvBuffer, 6 );
		
		if(dataSendBuffer.rear >= dataSendBuffer.front) {
			remainCount = dataSendBuffer.rear - dataSendBuffer.front;   
			///*
		//				printf("CH : dequeue 11 read count : %u\n", remainCount);
			//*/
		}
		// front가 큰 경우는 rear데이터가 버퍼의 끝을 넘어서 다시 처음으로 간 경우다. 
		else {
			remainCount = MAX_QUEUE_SIZE - dataSendBuffer.front + dataSendBuffer.rear;
			/*
			printf("CH : dequeue 22 read count : %u\n", remainCount);
			*/
		}
		
//			printf("save count : %u\n", remainCount);

		if((remainCount >= 100) && (dataSendBuffer.sending == false)) {
			dataSendBuffer.sending = true;
		}		
	}
	// mmm
	
	if( STOP_VIBE == true ) {
			stopVibeCount++;
			
			if( stopVibeCount >= 3 ) {
				stopVibeCount = 0;
				STOP_VIBE = false;
				
				printf("pwm_stop_A \n");
				
				pwm_stop_A();
			}
		}
	
	if( vibeNew == true ) {
			vibeNew = false;
			printf("in vibeNew \n");
			
			vibeData = vibePattern[ vibePatternIndex ];
			
			printf("in vibeNew, %x\n", vibeData);
			
			if( vibePattern[vibePatternIndex] != 0 ) {
				if( vibeData & 0x80 ) {
					isVibeOn = true;
					printf("in vibeNew, vibe ON \n");
				}
				else {
					isVibeOn = false;
					printf("in vibeNew, vibe OFF \n");
				}
				
				vibePatternCount = vibePattern[ vibePatternIndex ] & 0x7F;
				
				printf("in vibeNew, count : %d\n", vibePatternCount);
			}
			else {
				printf("in vibeNew 0 \n");
			}
		}
		
		
		if( vibePatternCount > 0 ) {
			if( isVibeOn == true ) {  // 진동을 해야한다.
				//IS_PWM_ENABLE = true;
				//PWM_ON = true;
				// TODO
				// 이 함수가 main()에 있는 상태에서 OLED에 전화번호가 scrolling되는 상황이면
				// 이 함수를 태울수없는 문제가 발생한다. 왜 그러지?			
				pwm_start_A();
			}
			else {                    // 진동을 멈춘다.
				//IS_PWM_ENABLE = true;
				//PWM_ON = false;
				pwm_stop_A();
			}
			
			vibePatternCount--;
			
			if( vibePatternCount <= 0 ) {
				
				isVibeOn = false;
				vibePatternIndex++;
				
				printf("get new vibe pattern %d\n", vibePatternIndex);
				
				if( vibePatternIndex < 15 ) {
					printf("get new vibe pattern < 15\n");
					vibeNew = true;				
				}
				else {
					printf("get new vibe pattern equal 15 or over\n");
					vibeNew = false;
					vibePatternIndex = 0;
					memset(&vibePattern, 0, sizeof(vibePattern));	
				}
			}
		}
		
	// 진동 preset 들어왔을때
	if( ING_PWM == true ) {
		if( motorStatusChangeCount == 0 ) {
			IS_PWM_ENABLE = true;
			PWM_ON = true;
			SEGGER_RTT_WriteString(0, "pwm first\n");
		}
		motorStatusChangeCount++;
		
		if( motorStatusChangeCount >= 10 ) {
			SEGGER_RTT_WriteString(0, "pwm end\n");
			
			if( motorStatusChangeCount == 10 ) {
				IS_PWM_ENABLE = true;
				PWM_ON = false;
			}
			
			motorStatusChangeCount = 0;			
			ING_PWM = false;							
		}
	}
	
	// FOR TEST
    // 액정 켜지는걸 보이위한 소스다. 일정시간마다 전체켜기, 전체끄기를 수행한다.
	showshowCount += 1;
	
	if( showshowCount >= 2 ) {
		showshowCount = 0;
		showshow = !showshow;
		
		updateTimeForTest(10);
	}
}

static void spi_send_recv(uint8_t * const p_tx_data,
                          uint8_t * const p_rx_data,
                          const uint16_t  len, bool rw_mode)
{
    // Start transfer.
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master_memory, p_tx_data, len, p_rx_data, len);
	
	if( err_code != 0 ) {
		printf("send_recv result : %u\n", err_code);
	}
	//printf("send_recv result : %u\n", err_code);
	//printf("spi rx : %x, %x, %x\n", p_rx_data[1], p_rx_data[2], p_rx_data[3]);
	
	/*
	if( rw_mode == RW_MODE_R ) {
		showReadData(len);
	}
	*/
	
    nrf_delay_ms(DELAY_MS_100);
}

static void spiInit() {
	uint32_t err_code;
	
	// Serial Flash
	nrf_drv_spi_config_t const config =
    {
		.sck_pin  = SPIM1_SCK_PIN,
		.mosi_pin = SPIM1_MOSI_PIN,
		.miso_pin = SPIM1_MISO_PIN,
		.ss_pin   = SPIM1_SS_PIN,
        .irq_priority = APP_IRQ_PRIORITY_LOW,
        .orc          = 0xCC,
        .frequency    = NRF_DRV_SPI_FREQ_1M,//NRF_DRV_SPI_FREQ_1M, // NRF_DRV_SPI_FREQ_500K, //NRF_DRV_SPI_FREQ_500K, //NRF_DRV_SPI_FREQ_1M, //NRF_DRV_SPI_FREQ_1M,
        .mode         = NRF_DRV_SPI_MODE_0,//NRF_DRV_SPI_MODE_0,
        .bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST, //NRF_DRV_SPI_BIT_ORDER_LSB_FIRST,				
    };		
	
    err_code = nrf_drv_spi_init(&m_spi_master_memory, &config, spi_master_memory_event_handler);	
	if( err_code != NRF_SUCCESS ) {
		SEGGER_RTT_printf(0, "spiInit : %d\n", err_code);
	}
}

static void spi_master_memory_event_handler(nrf_drv_spi_event_t event)
{
    uint32_t err_code = NRF_SUCCESS;
    bool result = false;
	
//	printf("event handler \n");

    switch (event)
    {
        case NRF_DRV_SPI_EVENT_DONE:
		//	printf("e1 handler, done \n");
            // Check if data are valid.

            //result = buf_check(m_rx_data, TX_RX_BUF_LENGTH);
            //APP_ERROR_CHECK_BOOL(result);

            err_code = bsp_indication_set(BSP_INDICATE_RCV_OK);
            APP_ERROR_CHECK(err_code);

            // Inform application that transfer is completed.
            m_transfer_completed_memory = true;
            break;

        default:
			//		printf("event handler, default \n");
            // No implementation needed.
            break;
    }
}

static void deviceInit() {
	flash_addr = FLASH_TARGET_ADDR;
	flash_addr_r = FLASH_TARGET_ADDR;
	
	passHandler = true; // 센서데이터를 메모리에 저장하지 않는다.
	SAVE_DATA = false;  // 센서데이터를 메모리에 저장하지 않는다.
	
	flashMap.flashPageChecker = 0;
	flashMap.pageNum = 0;
	flashMap.sectorNum = 0;
	flashMap.flashPageToRead = 0;
	
	isSaveData = false;
	readRun = false;    // 메모리에서 데이터를 읽어서 전송하지 않는다.
	
	clockInit(); // 밴드의 시간정보를 초기화한다.
	
	REAL_STEP = false;
	stepCheck = false;
	
	CMD_MEMORY_CLEAR = false;
	SHOW_CURRENT_TIME = false; // 현재시간을 보여준다.
	IS_CALL_NUM_SCROLLING = false;
	ING_PWM = false; // 진동(preset)이 들어왔는가?
	motorStatusChangeCount = 0;
	IS_PWM_ENABLE = false;
	PWM_ON = false;
	vibeNew = false;
	vibePatternIndex = 0;
	vibePatternCount = 0;
	isVibeOn = false;
	OLED_SHOWING = false;
	stopVibeCount = 0;
	
	GET_FIRM_VER = false;
	// iii
}

static void enqueue(char* data, uint16 len) { 
    /*uint16 iLoop = 0;*/
    int remainBuffer = 0;
    int remainLen = 0;
    int remainIndex = 0;
        
	printf("enqueue\n");
    
    /*
    SHOW_MSG_STR("enqueue : ");
    SHOW_MSG_UINT8(len);
    SHOW_MSG_STR("\r\n");
    */
     
    if(is_full()) 
    { 
        printf("Queue full\r\n");
    } 

	// 새로 넣는 데이터가 버퍼를 초과할 경우
    if(dataSendBuffer.rear + len - 1 >= MAX_QUEUE_SIZE) {
        //printf("enqueue 11\r\n");
        
        
        /*
        SHOW_MSG_STR("enqueue 11, rear is : ");
        SHOW_MSG_UINT16(dataSendBuffer.rear);
        SHOW_MSG_STR("\r\n");
        */
        /*
        for( iLoop = 0; iLoop < len; iLoop++) {
            //enqueueEach(data[iLoop]); 
        }
        */
        /*
        remainBuffer = MAX_QUEUE_SIZE - dataSendBuffer.rear + 1;
        SHOW_MSG_STR("remainBuffer is : ");
        SHOW_MSG_UINT8(remainBuffer);
        SHOW_MSG_STR("\r\n");
        
        */
        remainIndex = 0;
        remainLen = len;

        do {
            dataSendBuffer.rear = (dataSendBuffer.rear + 1) % MAX_QUEUE_SIZE;
            remainBuffer = MAX_QUEUE_SIZE - dataSendBuffer.rear + 1;
            
            /*
			printf("enqueue 11, remainBuffer is : %u\n", remainBuffer);
            */
            
            if( remainLen > remainBuffer ) {
                /*
                printf("do while if\n");				
                */
                memcpy(&dataSendBuffer.queue[dataSendBuffer.rear], &data[remainIndex], (char)remainBuffer);  
                dataSendBuffer.rear = dataSendBuffer.rear + remainBuffer - 1;
                
                /*
                printf("do while if rear is : %u\n", dataSendBuffer.rear);
                */
            
                remainIndex = remainIndex + remainBuffer;
                
                /*
                printf("do while if remainIndex is : %u\n", remainIndex);
                */
                
                remainLen = remainLen - remainBuffer;
                
                /*
                printf("do while if remainLen is : %u\n", remainLen);
                */
            }
            else { 
                /*
                printf("do while else....enqueue over *******\r\n");
                
                printf("do while else dataSendBuffer.rear 1 is : %u\n", dataSendBuffer.rear);
                */
                
                memcpy(&dataSendBuffer.queue[dataSendBuffer.rear], &data[remainIndex], (char)remainLen);  
                dataSendBuffer.rear = dataSendBuffer.rear + remainLen - 1;
                
                /*
                printf("do while else dataSendBuffer.rear 2 is : %u\n", dataSendBuffer.rear);                
                printf("do while else remainIndex is : %u\n", remainIndex);
                
                printf("do while else remainLen is : %u\n", remainLen);
                */
				
                remainLen = remainLen - remainLen;
                //dataSendBuffer.rear = dataSendBuffer.rear + len - 1;
            }
        } while( remainLen > 0 );
    }
	// 새로 넣는 데이터가 버퍼를 초과하지 않을 경우
    else {
//        printf("enqueue 22\r\n"); 
        dataSendBuffer.rear = (dataSendBuffer.rear + 1) % MAX_QUEUE_SIZE;
        
        ///*
//        printf("enqueue 22, rear index : %u\n", dataSendBuffer.rear);        
        //*/
            
        memcpy(&dataSendBuffer.queue[dataSendBuffer.rear], &data[0], (char)len);  
        dataSendBuffer.rear = dataSendBuffer.rear + len - 1;
    }
    
    ///*
//    printf("enque final rear : %u\n", dataSendBuffer.rear);                
//    printf("enque final front : %u\n", dataSendBuffer.front);
    //*/
                
    //dataSendTimerStart();
} 

static int is_full() { 
    uint16 val = 0;
    
    /*
    SHOW_MSG_STR("is_full check\r\n");
    */
    
    val = (dataSendBuffer.rear + 1) % MAX_QUEUE_SIZE;
    ///*
    printf("fullcheck,rear,front: %u, %u\n", val, dataSendBuffer.front);    	
    //*/
    
    return ((dataSendBuffer.rear + 1) % MAX_QUEUE_SIZE == dataSendBuffer.front);  
} 

static int is_empty() {
    /*I
    SHOW_MSG_STR("is_empty front : ");
    SHOW_MSG_UINT16(dataSendBuffer.front);
    SHOW_MSG_STR("\r\n");
    
    SHOW_MSG_STR("is_empty rare : ");
    SHOW_MSG_UINT16(dataSendBuffer.rear);
    SHOW_MSG_STR("\r\n");
    */
    return (dataSendBuffer.front == dataSendBuffer.rear);  
} 

static void dequeue() {  
    int count = 0;
    int remainLen = 0;
    int remainIndex = 0;
    int remainToReadCount = 0;
//	int iLoop = 0;
    
    if(is_empty()) 
    { 
        printf("Queue empty\n");
    } 	
    
	// 읽어올 수 있는 데이터 수를 알아낸다.
	// rear가 큰 경우는 rear-front가 읽어갈 수 있는 데이터의 수이다. 
    if(dataSendBuffer.rear >= dataSendBuffer.front) {
        count = dataSendBuffer.rear - dataSendBuffer.front;   
        /*
        //printf("dequeue 11 read count : %u\n", count);
        */
    }
	// front가 큰 경우는 rear데이터가 버퍼의 끝을 넘어서 다시 처음으로 간 경우다. 
    else {
        count = MAX_QUEUE_SIZE - dataSendBuffer.front + dataSendBuffer.rear;
        /*
        //printf("dequeue 22 read count : %u\n", count);
        */
    }
	
	printf("Run dequeue \n");
	
	if( count >= 36 ) {		        
		//printf("Run dequeue, over 36 \n");
		/*
		buffer 잔량 체크를 위한 로그
		if(( count > 900 ) && ( count < 1100)) {
			SHOW_MSG_STR("enque buffer == 900 ~ 1000\r\n");    
		}
		else if( ( count > 1900 ) && ( count < 2100 ) ) {
			SHOW_MSG_STR("enque buffer == 1900 ~ 2100 \r\n");    
		}
		else if( (count > 2900 ) && (count < 3100 )) {
			SHOW_MSG_STR("enque buffer == 2900 ~ 3100\r\n");    
		}
		else if( count > 3800 ) {
			SHOW_MSG_STR("enque buffer >  3800\r\n");    
		}
		*/
		
		// 센서 데이터 x, y, z가 각각 6바이트다. 
		// 이걸 한번에 6묶음을 쓰려고 한다. 그래서 36바이트다. 
		/*
		if( count > 20 ) {
			count = 20;
		}
		*/
		
		// flash page에 저장하기 위한 데이터수를 결정하는 부분
		// 기본적으로는 36바이트다. 센서 데이터(x, y, z가 각각 2바이트) 1회 분량이
		// 6바이트이고, 6묶음해서 총 36바이트		
		if( (flashMap.flashPageChecker + 36) > 256 ) {
			flashMap.flashPageToRead = 256 - flashMap.flashPageChecker;			
			//printf("### 00 flashPageToRead : %u\n", flashMap.flashPageToRead);
		}
		// pageChecker가 255라는건 page의 단위가 바뀐다는 말이다. 따라서 36을 다 읽어와야한다. 
		else if( flashMap.flashPageChecker == 256) {
			flashMap.flashPageChecker = 0;
			flashMap.flashPageToRead = 36;
			//printf("### 01 flashPageToRead : %u\n", flashMap.flashPageToRead);
		}
		else {
			flashMap.flashPageToRead = 36;
			//printf("### 02 flashPageToRead : %u\n", flashMap.flashPageToRead);
		}
		
		//printf("flashPageToRead, checker : %u, %u\n", flashMap.flashPageToRead, flashMap.flashPageChecker);
		
		/*
		flashMap.flashPageToRead = FLASH_PAGE_MAX - flashMap.flashPageChecker;
		if( flashMap.flashPageToRead >= 36 ) {
			flashMap.flashPageToRead = 36;
		}
		*/
		
		// 이하 코드가 count로부터 읽어들이게 되어있어서 값을 넣어준다. 
		// refactoring 필요
		count = flashMap.flashPageToRead;
		
		///*
		//printf("dequeue front : %u\n", dataSendBuffer.front);
		//*/
		
		// TODO 리팩토링
		/*
		for( iLoop = 0; iLoop < 20; iLoop++) {
			dataSendBuffer.sBufferTemp[iLoop] = 0;
		}
		*/
		
		// 플래시에 쓰려는 데이터는 36바이트이지만, 앞에 커맨드와 addr가 붙기때문에
		// 40바이트를 초기화한다. 
		memset(dataSendBuffer.sBufferTemp, 0, 40);
		
		// 읽으려는 데이터가 버퍼를 초과한다. 
		if(dataSendBuffer.front + count - 1 >= MAX_QUEUE_SIZE) {
			remainIndex = 4;
			remainLen = count;  
			
			///*
			//printf("dequeue 777 remainLen : %u\n", remainLen);
			//*/
		
			do {
				// 읽으려는 수가 맥스버퍼를 초과하기때문에 맥스버퍼에서 읽어올 수 있는 수를 계산한다.
				// remainToReadCount는 현재 버퍼에서 내가 읽일 수 있는 만큼의 수를 가진다.
				remainToReadCount = MAX_QUEUE_SIZE - dataSendBuffer.front;
				dataSendBuffer.front = (dataSendBuffer.front + 1) % MAX_QUEUE_SIZE;
				
			   // aaac = aaac + 1;
				
				/*            
				SHOW_MSG_STR("dequeue 777-8 aaac : ");
				SHOW_MSG_UINT16(aaac);
				SHOW_MSG_STR("\r\n");
				*/
				
				///*
				//printf("dequeue 777-1 remainToReadCount : %u\n", remainToReadCount);            
				//printf("dequeue 777-2 front : %u\n", dataSendBuffer.front);
				//*/
				
				// refactoring
				// 이 부분은 필요없어 보인다. 이미 위에서 읽어올 수보다 남은 수가 적다는게
				// 판단이 되어있기때문이다. 
				if( remainToReadCount >= remainLen) {
					///*
					//printf("dequeue 777-3 remainLen : %u\n", remainLen);
					//*/
					remainToReadCount = remainLen;
				}
				
				// 읽어온 카운트가 이미 맥스버퍼를 가리키고 있으면서 읽어올 데이터는 아직 남아있다. 
				if( (remainToReadCount == 0) && (remainLen > 0) ) {
					remainToReadCount = remainLen;
					
					///*
					//printf("dequeue 777-3-1 remainLen : %u\n", remainLen);
					//*/
									
					if( remainToReadCount >= 36 ) {
						remainToReadCount = 36;
						///*
						//printf("dequeue 777-3-2 remainToReadCount : %u\n", remainToReadCount);
						//*/
					}
				}
				
				/*
				SHOW_MSG_STR("*** sBuffer : ");
				for( i = 0; i < remainToReadCount; i++ ) {
					SHOW_MSG_CHAR( dataSendBuffer.queue[i] );         
				}
				SHOW_MSG_STR("..\r\n");
				*/
				
				///*
				//printf("dequeue 777-4-1 remainIndex : %u\n", remainIndex);            
				//printf("dequeue 777-4 remainToReadCount : %u\n", remainToReadCount);            
				//printf("dequeue 777-4 front : %u\n", dataSendBuffer.front);
				//*/
							
				// flash에 기록할 데이터를 뽑아온다.
				memcpy(&dataSendBuffer.sBufferTemp[remainIndex], &dataSendBuffer.queue[dataSendBuffer.front], remainToReadCount);
				remainIndex = remainIndex + remainToReadCount;
				flashMap.flashPageChecker += remainToReadCount;
				printf("checkcheck 00 : %u\n", flashMap.flashPageChecker);
				
		
				/*
				SHOW_MSG_STR("dequeue 777-4-2 remainLen : ");
				SHOW_MSG_UINT16(remainLen);
				SHOW_MSG_STR("\r\n");
				
				SHOW_MSG_STR("dequeue 777-4-3 remainToReadCount : ");
				SHOW_MSG_UINT16(remainToReadCount);
				SHOW_MSG_STR("\r\n");
				*/
				
				remainLen = remainLen - remainToReadCount;
				
				/*
				SHOW_MSG_STR("dequeue 777-5 remainIndex : ");
				SHOW_MSG_UINT16(remainIndex);
				SHOW_MSG_STR("\r\n");
				
				SHOW_MSG_STR("dequeue 777-6 remainLen : ");
				SHOW_MSG_UINT16(remainLen);
				SHOW_MSG_STR("\r\n");
				*/
				
				dataSendBuffer.front = dataSendBuffer.front + remainToReadCount - 1;
				
				/*
				SHOW_MSG_STR("dequeue 777-7 front : ");
				SHOW_MSG_UINT16(dataSendBuffer.front);
				SHOW_MSG_STR("\r\n");
				*/
				
				/*
						if( aaac == 2) {
					SHOW_MSG_STR("xxxxxxx aaac is 2\r\n ");
					break;          
				}
				*/
				

				
			} while( remainLen > 0 );
			
			/*
			for(iLoop = 0; iLoop < count; iLoop++) {
				maxValue = dataSendBuffer.front + 1;
				
				if( maxValue == 40 ) {
					dataSendBuffer.front = 40; //(dataSendBuffer.front + 1 + 1) % MAX_QUEUE_SIZE;
				}
				else {
					dataSendBuffer.front = (dataSendBuffer.front + 1) % MAX_QUEUE_SIZE;
				}
				
				dataSendBuffer.sBufferTemp[iLoop] = dequeueEach();
				
			}        
			*/
		}
		// 읽으려는 데이터가 max버퍼를 넘지 않는다. 즉, 한번에 읽을 수 있다.
		else {
			dataSendBuffer.front = (dataSendBuffer.front + 1) % MAX_QUEUE_SIZE;
			
			// 남은 잔량이 내가 읽으려는 수보다 크다. 즉, 한번에 읽을 수 있다.          
			//printf("dequeue read at once, %u\n", flashMap.flashPageToRead);
			memcpy(&dataSendBuffer.sBufferTemp[4], &dataSendBuffer.queue[dataSendBuffer.front], flashMap.flashPageToRead);
			count = flashMap.flashPageToRead;
			
			flashMap.flashPageChecker += count;
			//printf("checkcheck 11 : %u\n", flashMap.flashPageChecker);

			dataSendBuffer.front = dataSendBuffer.front + count - 1;
		}
		
		///*    
	//	printf("in dequeue 10 \n");
		
		/*
		for( iLoop = 0; iLoop < 4; iLoop++ ) {
			printf("*** sBuffer : %c, %c, %c, %c, %c, %c, %c, %c, %c, %c\n", dataSendBuffer.sBufferTemp[iLoop * 10], dataSendBuffer.sBufferTemp[iLoop * 10 + 1] 
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 2], dataSendBuffer.sBufferTemp[iLoop * 10 + 3], dataSendBuffer.sBufferTemp[iLoop * 10 + 4]
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 5], dataSendBuffer.sBufferTemp[iLoop * 10 + 6], dataSendBuffer.sBufferTemp[iLoop * 10 + 7]
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 8], dataSendBuffer.sBufferTemp[iLoop * 10 + 9]);         
		}
		*/
		
		printf("in dequeue \n");
		/*
		for( iLoop = 0; iLoop < 4; iLoop++ ) {
			printf("*** sBuffer : %x, %x, %x, %x, %x, %x, %x, %x, %x, %x\n", dataSendBuffer.sBufferTemp[iLoop * 10], dataSendBuffer.sBufferTemp[iLoop * 10 + 1] 
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 2], dataSendBuffer.sBufferTemp[iLoop * 10 + 3], dataSendBuffer.sBufferTemp[iLoop * 10 + 4]
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 5], dataSendBuffer.sBufferTemp[iLoop * 10 + 6], dataSendBuffer.sBufferTemp[iLoop * 10 + 7]
			, dataSendBuffer.sBufferTemp[iLoop * 10 + 8], dataSendBuffer.sBufferTemp[iLoop * 10 + 9]);         
		}
		*/
		
		//printf("in dequeue 11 %c\n", dataSendBuffer.sBufferTemp[4]);
		//printf("in dequeue 12 %u\n", dataSendBuffer.sBufferTemp[4]);
		
		/*
		printf("in dequeue  22 \n");
		printf("in dequeue  22 \n");
		printf("in dequeue  22 \n");
		printf("in dequeue  22 \n");
		printf("in dequeue  22 \n");
		
		printf("in dequeue  22 \n");
		*/
		
		/*
		SHOW_MSG_STR("dequeue read count : ");
		SHOW_MSG_UINT16(count);
		SHOW_MSG_STR("\r\n");
		*/
		
		/*
		// 데이터를 사용하면 된다.
		GattCharValueNotification(appData.connectedUcid,
											   HANDLE_UART_WRITE_VALUE,
											   count,
											   (uint8*)dataSendBuffer.sBufferTemp); 
		*/
		
		if( (flash_addr + flashMap.flashPageToRead) > FLASH_TARGET_SIZE ) {
			printf("+full, stop recording\n");
			
			// 메모리가 풀이면 더이상 기록하지 않는다.
			passHandler = true;  // 더 이상 모션센서를 기록하지 않는다.
			readRun = false;     // 메모리에서 값을 읽어서 보내지 않는다.
			
			notiFlashWriteFull(); // 메모리가 꽉 찼음을 앱에 알린다.
		}
		else {
			SAVE_DATA = true;
			isSaveData = true;
		}
		
		
		
		if(is_empty()) 
		{ 
			dataSendBuffer.sending = FALSE;
		} 
	}
	else {
		dataSendBuffer.sending = FALSE;
		printf("Rundequeue, less than 36\n");
	}		
} 

static void save_timeout_handler(void * p_context) {
	uint16 remainCount = 0;
	uint16 err_code;
	
//	printf("******* save timeout *******\n");
	
	if( dataSendBuffer.sending == true ) {
		if( isSaveData == false) {
			if(is_empty()) { 
				printf("Queue is empty\n");
			}
			else {
				if(dataSendBuffer.rear >= dataSendBuffer.front) {
					remainCount = dataSendBuffer.rear - dataSendBuffer.front;   
					///*
				//				printf("CH : dequeue 11 read count : %u\n", remainCount);
					//*/
				}
				// front가 큰 경우는 rear데이터가 버퍼의 끝을 넘어서 다시 처음으로 간 경우다. 
				else {
					remainCount = MAX_QUEUE_SIZE - dataSendBuffer.front + dataSendBuffer.rear;
					/*
					printf("CH : dequeue 22 read count : %u\n", remainCount);
					*/
				}
				
				printf("save count : %u\n", remainCount);

				if(remainCount >= 36) {
					//dataSendBuffer.sending = TRUE;  
					printf("save - run dequeue \n");		
					dequeue();
					
					///*
					err_code = app_timer_stop(save_timer_id);
					//APP_ERROR_CHECK(err_code);
					
					err_code = app_timer_start(save_timer_id, MAIN_TIMER_INTERVAL, NULL);
					printf("start : %d\n", err_code);	
					//*/
					
				}	
				else {
					dataSendBuffer.sending = FALSE;  
					//printf("* save timeout, noting < 20 *\n");
				}
			} 
		}
		else {
		//	printf("save handler : sending is true, saving is processing\n");
		}			
	}
	else {
//		printf("save handler : sending is FALSE\n");
	}
	
}

static void notiFlashReadFull() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xCD;
	row_data[2] = 0x1;
	row_data[3] = 0x7;  // flash read full

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("ReadFull: %u\n", err_code);	
}

static void notiFlashReadLast() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xCD;
	row_data[2] = 0x1;
	row_data[3] = 0x8;  // flash read last

	getChecksum(row_data, row_data[2] + 3, &acca );
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("send, notiFlashReadLast : %u\n", err_code);	
}

static void getChecksum(char* value, uint8 size, uint16 *total) {
 //   int checksum = 0;
//    return checksum;
//    /*
    uint8 bb = size;
    uint8 i = 0;
    uint16 sum = 0;
//    uint8 checksum = 0;
//    uint8 cs = 0;
    
    //printf("check loop size : %d\n", size);   
 
	for (i = 0; i < bb; i++) {
		sum += value[i];
        
        //printf("check sum : %d\n", sum);
	}
    
    //printf("tatal check sum : %d\n", sum);
    
    *total = sum;
}

static void notiFlashWriteFull() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xCD;
	row_data[2] = 0x1;
	row_data[3] = 0x6;  // flash write full

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("hWriteFull: %u\n", err_code);	
}

static void noti_timeout_handler(void * p_context) {
	uint16 err_code;
	
	printf("noti_timeout_handler 1 \n");
	
	if( (dataTimeStamp.time_hh == 0xff ) && (dataTimeStamp.time_mm == 0xff) ) {
		SEGGER_RTT_WriteString(0, "clock is not set \n");
	}
	else {
		//updateTime(10);
	}
	
	//LEDS_OFF(BSP_LED_3_MASK);
	
	//err_code = app_timer_stop(noti_timer_id);
//	printf("noti_timeout_handler, %d\n", err_code);
}

// 밴드의 시간정보를 초기화한다.	
static void clockInit() {
	dataTimeStamp.time_year = 0x0;
	dataTimeStamp.time_month = 0x0;
	dataTimeStamp.time_day = 0x0;
	dataTimeStamp.time_hh = 0x0;
	dataTimeStamp.time_mm = 0x0;
	dataTimeStamp.time_ss = 0x0;
	dataTimeStamp.data_count = 0;	
	
	dataTimeStamp.timeTotalBackup = 0;
	dataTimeStamp.timeTotal = 0;
}

static void sensorModeChange(uint8 mode) {
    //printf("mode change \r\n");
    
	// 공통 설정
	REAL_STEP = false;	
	
	// 4는 실시간 걸음수다. 일단 기본적으로는 스텝카운트 체크가 기본이고,
	// 걸음수가 올라가면 BT로 올라간 수를 전송하는것만 추가하면 된다.
    if( mode == 0 || mode == 4 ) { // 스텝카운트
        printf("stepcount \r\n");
		
		passHandler = true;
		readRun = false;
		stepCheck = true;
        //sensorMode = 0;
        //TimerDelete(accTimerID);
        
        //readTest();					
		
		if( mode == 4 ) {
			printf("mode : realtime step count\n");		
			REAL_STEP = true;			
		}
    }
    else if( mode == 1 ) { // 실시간 raw data 측정
        printf("realtime \r\n");
        //sensorMode = 1;
        //TimerDelete(accTimerID);
        //accTimerID = TimerCreate(ACC_TIMER, TRUE, accTimerHandler);
        
       //   writeTest();
				
		
		passHandler = true;
		readRun = false;
		stepCheck = false;
    }
    else if( mode == 2 ) { // 수면모드
        //sensorMode = 2;
        //TimerDelete(accTimerID);
        printf("sleep \r\n");
		
		passHandler = true;
		readRun = false;
		stepCheck = false;
        
        //writeTest(); // 빌드가 되지 않아서 여기에서 한 번 호출함. 의미없는 코드임
    }
    else if( mode == 3 ) { // 실시간 raw data 메모리에 저장
		printf("sleep save \r\n");
		
		readRun = false;
		passHandler = false;
		//stepCheck = false;
		
		// 
		
		
      //  if( sensorMode != 3 ) {
        //    sensorMode = 3;
        
           // DEBUG_MSG_STR("mode change to raw to ext : ");
          ////  DEBUG_MSG_UINT16( g_acc_data.dataSize );
          //  DEBUG_MSG_STR(" .\r\n ");
            
            
          //  TimerDelete(accTimerID);
           // accTimerID = TimerCreate(ACC_TIMER, TRUE, accTimerHandler);
       // }
        //else {
          //  printf("already mode 3 \r\n");
            //statusNoti(0x4);
        //}
    }
}

static void savedDataSend(void) {
    printf("savedDataSend \r\n");
	
	readRun = true;
    
    /*
    DEBUG_MSG_STR("accExtTimer : ");
    DEBUG_MSG_UINT16(g_acc_data.dataSize);
    DEBUG_MSG_STR(" , ");
    DEBUG_MSG_UINT16(g_acc_data.dataSizeRead );
    DEBUG_MSG_STR(" .. \r\n");
    */    
    
	/*
    if( g_acc_data.dataSize > g_acc_data.dataSizeRead ) {        
        TimerDelete(accExtTimerID);
        accExtTimerID = TimerCreate(ACC_TIMER, TRUE, accExtTimerHandler);
    }
    else {
        DEBUG_MSG_STR("no data for send \r\n");
        statusNoti(0x3);
    }
	*/
	
	passHandler = true;
}

static void savedDataClear(void) {
	uint8 iLoop;
	
	printf("Clear \r\n");
	
	/*
	ssd1306_clear_screen(0x00);
	ssd1306_display_string(10, 10, "Clear...", 12, 1);
	ssd1306_refresh_gram( DIS_MULTI_BYTE );	
	*/
	nrf_delay_ms(DELAY_MS + 30);
	
	
    flash_addr = FLASH_TARGET_ADDR;
	flash_addr_r = FLASH_TARGET_ADDR;
	
	
	///*
	// Block Erase(64K)
	// 64M 전체 블럭을 삭제한다.
	for( iLoop = 0; iLoop < 128; iLoop++) {	 //
		m_tx_data[0] = 0x06;
		spi_send_recv(m_tx_data, m_rx_data, 1, RW_MODE_W ); // TX_RX_BUF_LENGTH
		nrf_delay_ms(DELAY_MS);
		
		m_tx_data[0] = 0xD8;		
		m_tx_data[1] = flash_addr >> 16;
		m_tx_data[2] = flash_addr >> 8;
		m_tx_data[3] = flash_addr;	
		
		printf("block add : %d, %x, %x, %x\n", iLoop, m_tx_data[1], m_tx_data[2], m_tx_data[3]);
		spi_send_recv(m_tx_data, m_rx_data, 4, RW_MODE_W ); // TX_RX_BUF_LENGTH		
		
		flash_addr += 0x10000;
		nrf_delay_ms(DELAY_MS);
	}			  

	flash_addr = FLASH_TARGET_ADDR;
	
	notiFlashEraseComplete();
	sensorModeChange(0);
	//SHOW_OLED_STEP_FLAG = true;  // 전체 메모리 초기화후 스텝카운트를 보여주도록 하자.
	//*/
}

static void notiFlashEraseComplete() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xCD;
	row_data[2] = 0x1;
	row_data[3] = 0x5;  // flash full erase complete

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[4] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("EraseComplete: %u\n", err_code);	
}

static void oledPinInit(uint32_t dc, uint32_t rs, uint32_t cs, uint32_t clk, uint32_t mosi) {
	_dc = dc;
	_rs = rs;
	_cs = cs;
	
	nrf_gpio_cfg_output(dc);
    nrf_gpio_cfg_output(rs);
    _HI_CS();
    nrf_gpio_cfg_output(rs);
	
	// OLED
	nrf_drv_spi_config_t const config2 =
    {
		.sck_pin  = clk,
		.mosi_pin = mosi,
		.miso_pin = NRF_DRV_SPI_PIN_NOT_USED, //dc, //NRF_DRV_SPI_PIN_NOT_USED, //dc,//NRF_DRV_SPI_PIN_NOT_USED,//SPIM2_MISO_PIN,
		.ss_pin   = NRF_DRV_SPI_PIN_NOT_USED, //cs,//NRF_DRV_SPI_PIN_NOT_USED,//SPIM2_SS_PIN,
        .irq_priority = APP_IRQ_PRIORITY_LOW,
        .orc          = 0xCC,
        .frequency    = NRF_DRV_SPI_FREQ_1M, //NRF_DRV_SPI_FREQ_8M, //NRF_DRV_SPI_FREQ_500K, //NRF_DRV_SPI_FREQ_1M, //NRF_DRV_SPI_FREQ_1M,
        .mode         = NRF_DRV_SPI_MODE_3,//NRF_DRV_SPI_MODE_0,
        .bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST, //NRF_DRV_SPI_BIT_ORDER_LSB_FIRST,
    };
	ret_code_t err_code = nrf_drv_spi_init(&m_spi_master_oled, &config2, spi_master_oled_event_handler);	
	
	oledICReset();
	oledInit();
}
static void oledICReset() {
	_LO_RS();
	nrf_delay_ms(50);
	_HI_RS();
}

static void oledInit() {
	SEGGER_RTT_WriteString(0, "oledInit start\n"); 
	/*
	//동작잘되는 셋팅 1
	oledWriteCommand(0xAE);     //Set Display Off

	oledWriteCommand(0xd5);     //display divide ratio/osc. freq. mode
	oledWriteCommand(0xd1);     //105Hz	  AU²¿¾§On470 KHz

	oledWriteCommand(0xA8);     //multiplex ration mode:31
	oledWriteCommand(0x1F);		//32

	oledWriteCommand(0xD3);     //Set Display Offset
	oledWriteCommand(0x00);

	oledWriteCommand(0x40);     //Set Display Start Line

	oledWriteCommand(0x8D);     //Set Display Offset
	//oledWriteCommand(0x10);		//disable charge pump
	oledWriteCommand(0x14);	//for enabling charge pump

	oledWriteCommand(0xA1);     //Segment Remap

	oledWriteCommand(0xC8);     //Sst COM Output Scan Direction

	oledWriteCommand(0xDA);     //common pads hardware: alternative
	oledWriteCommand(0x12);

	oledWriteCommand(0x81);     //contrast control 
	oledWriteCommand(0xcf);		// AU²¿0xCF


	oledWriteCommand(0xD9);	    //set pre-charge period
	oledWriteCommand(0xF1);

	oledWriteCommand(0xDB);     //VCOM deselect level mode
	oledWriteCommand(0x40);	    //set Vvcomh=0.83*Vcc

	oledWriteCommand(0xA4);     //Set Entire Display On/Off

	oledWriteCommand(0xA6);     //Set Normal Display

	oledWriteCommand(0xAF);     //Set Display On	
	*/
	///*
	oledWriteCommand(0xAE);//--turn off oled panel
	oledWriteCommand(0x00);//---set low column address
	oledWriteCommand(0x10);//---set high column address
	oledWriteCommand(0x40);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
	oledWriteCommand(0x81);//--set contrast control register
	oledWriteCommand(0xCF);// Set SEG Output Current Brightness
	oledWriteCommand(0xA1);//--Set SEG/Column Mapping     
	oledWriteCommand(0xC0);//Set COM/Row Scan Direction   
	oledWriteCommand(0xA6);//--set normal display
	oledWriteCommand(0xA8);//--set multiplex ratio(1 to 64)
	oledWriteCommand(0x3f);//--1/64 duty
	oledWriteCommand(0xD3);//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
	oledWriteCommand(0x00);//-not offset
	oledWriteCommand(0xd5);//--set display clock divide ratio/oscillator frequency
	oledWriteCommand(0x80);//--set divide ratio, Set Clock as 100 Frames/Sec
	oledWriteCommand(0xD9);//--set pre-charge period
	oledWriteCommand(0xF1);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
	oledWriteCommand(0xDA);//--set com pins hardware configuration
	oledWriteCommand(0x12);
	oledWriteCommand(0xDB);//--set vcomh
	oledWriteCommand(0x40);//Set VCOM Deselect Level
	oledWriteCommand(0x20);//-Set Page Addressing Mode (0x00/0x01/0x02)
	oledWriteCommand(0x02);//
	oledWriteCommand(0x8D);//--set Charge Pump enable/disable
	oledWriteCommand(0x14);//--set(0x10) disable
	oledWriteCommand(0xA4);// Disable Entire Display On (0xa4/0xa5)
	oledWriteCommand(0xA6);// Disable Inverse Display On (0xa6/a7) 
	oledWriteCommand(0xAF);//--turn on oled panel
	//*/
	SEGGER_RTT_WriteString(0, "oledInit end\n"); 
}



static void spi_master_oled_event_handler(nrf_drv_spi_event_t event)
{	
    switch (event)
    {
        case NRF_DRV_SPI_EVENT_DONE:
            // Inform application that transfer is completed.
			bsp_indication_set(BSP_INDICATE_RCV_OK);
            
            m_transfer_completed_oled = true;
            break;

        default:
            break;
    }
}

static void oledWriteCommand(unsigned char chData) {
	///*
	_HI_CS();
	_LO_DC();
	_LO_CS();
	//*/
/*
	_LO_CS();
	_LO_DC();
	*/
	
	//m_transfer_completed_oled = false;
	//SEGGER_RTT_WriteString(0, "oledWriteCommand 11\n"); 
	spi_transfer_oled(&chData, 1);
	//SEGGER_RTT_WriteString(0, "oledWriteCommand 22\n"); 
	//while (m_transfer_completed_oled == false);

	_HI_CS();	
}

static void oledWriteData(unsigned char date)
{
	
	/*
	_LO_CS();
	_HI_DC();
	*/
	_HI_CS();
    _HI_DC();
    _LO_CS();

	spi_transfer_oled(&date, 1);
	
	_HI_CS();

	// ** μo·¹AI CE¿a **
	//delay_us(1);

	//_HI_DC();
	
}

uint8_t* spi_transfer_oled(uint8_t * message, const uint16_t len)
{
    memcpy((void*)m_tx_data, (void*)message, len);
	//SEGGER_RTT_WriteString(0, "spi_transfer_oled 11\n"); 
    spi_send_recv_oled(m_tx_data, m_rx_data, len);
	//SEGGER_RTT_WriteString(0, "spi_transfer_oled 22\n"); 
    return m_rx_data;
}

void spi_send_recv_oled(uint8_t * const p_tx_data,
                   uint8_t * const p_rx_data,
                   const uint16_t len)
{
    m_transfer_completed_oled = false;
    
	//SEGGER_RTT_WriteString(0, "spi_send_recv_oled 11\n");
	
//	SEGGER_RTT_WriteString(0, "se 1\n");
	// Start transfer.
    uint32_t err_code = nrf_drv_spi_transfer(&m_spi_master_oled, p_tx_data, len, p_rx_data, len);
	
//	SEGGER_RTT_WriteString(0, "se 2\n");
	
	//SEGGER_RTT_printf(0, "spi_send_recv_oled 22, %d\n", err_code);
	
	if( err_code != NRF_SUCCESS ) {
		SEGGER_RTT_printf(0, "*** spi trans : %d\n", err_code);
	}
	
    while (!m_transfer_completed_oled);
}

void Clear_Screen(unsigned char chFill)
{
   uint8 page_number,column_number;
   for(page_number=MIN_OLED;page_number<PAGE_TOTAL;page_number++)
   {
     oledWriteCommand(START_PAGE+page_number);
     oledWriteCommand(START_HIGH_BIT);
     oledWriteCommand(START_LOW_BIT);
	   /*
     for(column_number=MIN_OLED;column_number<COLUMN_MAX;column_number++)
     {
        oledWriteData(0xFF);
     }
	   */
	   for(column_number=MIN_OLED;column_number<COLUMN_MAX;column_number++)
     {
        oledWriteData(chFill);
     }
   }
}

void Clear_Screen_test(unsigned char chFill)
{
   uint8 page_number,column_number;
   for(page_number=MIN_OLED;page_number<PAGE_TOTAL;page_number++)
   {
     oledWriteCommand(START_PAGE+page_number);
     oledWriteCommand(START_HIGH_BIT);
     oledWriteCommand(START_LOW_BIT);
	   /*
     for(column_number=MIN_OLED;column_number<COLUMN_MAX;column_number++)
     {
        oledWriteData(0xFF);
     }
	   */
	   for(column_number=MIN_OLED;column_number<COLUMN_MAX;column_number++)
     {
        oledWriteData(chFill);
		 nrf_delay_ms(100);
     }
   }
}

void ssd1306_clear_screen(unsigned char chFill)  
{ 
	unsigned char i, j;

	for (i = 0; i < 4; i ++) 
	{
		for (j = 0; j < 64; j ++)  // max 64
		{
			s_chDispalyBuffer[j][i] = chFill;
		}
	}

	ssd1306_refresh_gram();
}

void ssd1306_clear_data(unsigned char chFill)  
{ 
	unsigned char i, j;

	for (i = 0; i < 4; i ++) 
	{
		for (j = 0; j < 64; j ++)  // max 64
		{
			s_chDispalyBuffer[j][i] = chFill;
		}
	}
}

void ssd1306_refresh_gram(void)
{
	unsigned char i, j;
	
	if( OLED_SHOWING == false ) {		
		OLED_SHOWING = true;

		for (i = 0; i < 4; i ++) 
		{  
			//SEGGER_RTT_printf(0, "gram %d start\n", i);
			oledWriteCommand(0xB0 + i);    
			//oledWriteCommand(0x00); 
			//oledWriteCommand(0x10); 
			nrf_delay_ms(1);
			oledWriteCommand(0x12); 
			nrf_delay_ms(1);
			oledWriteCommand(0x00); 
			nrf_delay_ms(3);
	 
			//SEGGER_RTT_WriteString(0, "gram data s\n");
			
			/*
			_HI_CS();
			_HI_DC();
			_LO_CS();
			*/
		//	_LO_CS();
		//	_HI_DC();
			
			for (j = 0; j < 63; j ++) {  // max 64
				oledWriteData(s_chDispalyBuffer[j][i]); 
				//nrf_delay_ms(1);
			}
			
			nrf_delay_ms(DELAY_MS);
			
		//	_HI_CS();
			
			//SEGGER_RTT_printf(0, "gram %d end\n", i);
		}

		OLED_SHOWING = false;		
	}
	else {
		SEGGER_RTT_WriteString(0, "********* showing &&&&&&&\n");
	}
}



/* 동작되는 data write
static void oledWriteData(unsigned char date)
{
	uint8 i,value;
	value = date;

	_LO_CS();
	_HI_DC();

	for(i=0;i<2;i++)
	{
		spi_transfer_oled(&date, 1);
	}

	_HI_CS();
}
*/

void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
	//printf("pwm_ready_callback \n");
    ready_flag = true;
}

void pwm_stop_A(void) {
	printf("pwm_stop_A\n");
	/*
   nrf_gpio_pin_clear(MOTOR_A_EN_PIN);
   app_pwm_disable(&PWM1);
   nrf_drv_gpiote_out_task_disable(MOTOR_A_PWM_PIN);
   nrf_gpio_cfg_output(MOTOR_A_PWM_PIN);
   nrf_gpio_pin_clear(MOTOR_A_PWM_PIN);
   //pwmAEnabled=false;	
	*/
	
	nrf_gpio_pin_clear(MOTOR_A_EN_PIN);
}

void pwm_start_A(void) {
	printf("pwm_start_A\n");
	nrf_gpio_pin_set(MOTOR_A_EN_PIN);
    /*
	nrf_drv_gpiote_out_task_enable(MOTOR_A_PWM_PIN); 
    app_pwm_enable(&PWM1);
    //pwmAEnabled=true;	
	
	//while (app_pwm_channel_duty_set(&PWM1, 1, 5) == NRF_ERROR_BUSY);
	//app_pwm_channel_duty_set(&PWM1, 1, 5);
	while (app_pwm_channel_duty_set(&PWM1, 1, 5) == NRF_ERROR_BUSY);
	*/
	STOP_VIBE = true;	
}

void ssd1306_draw_0608char(unsigned char chXpos, unsigned char chYpos, unsigned char chChar)
{
	unsigned char i, j;
	unsigned char chTemp = 0, chXpos0 = chXpos, chMode = 0;

	for (i = 0; i < 8; i ++)
	{
		chTemp = c_chFont0608[chChar - 0x30][i];
		
		for (j = 0; j < 8; j ++) 
		{
			chMode = chTemp & 0x80? 1 : 0; 
			ssd1306_draw_point(chXpos, chYpos, chMode);
			chTemp <<= 1;
			chXpos ++;
			
			if ((chXpos - chXpos0) == 6) 
			{
				chXpos = chXpos0;
				chYpos ++;
				break;
			}
		}
	}
}

void ssd1306_draw_point(unsigned char chXpos, unsigned char chYpos, unsigned char chPoint)
{
	unsigned char chPos, chBx, chTemp = 0;

	if (chXpos > 127 || chYpos > 63) 
		return;

	chPos = 7 - chYpos / 8;
	chBx = chYpos % 8;
	chTemp = 1 << (7 - chBx);

	if (chPoint) 
		s_chDispalyBuffer[chXpos][chPos] |= chTemp;
	else 
		s_chDispalyBuffer[chXpos][chPos] &= ~chTemp;
}

void ssd1306_display_string(unsigned char chXpos, unsigned char  chYpos, const unsigned char  *pchString, unsigned char  chSize, unsigned char  chMode)
{
	while (*pchString != '\0')
	{       
		if (chXpos > (SSD1306_WIDTH - chSize / 2)) 
		{
			chXpos = 0;
			chYpos += chSize;
			if (chYpos > (SSD1306_HEIGHT - chSize))
			{
				chYpos = chXpos = 0;
				//ssd1306_clear_screen(0x00);
			}
		}

		ssd1306_display_char(chXpos, chYpos, *pchString, chSize, chMode);
		
		chXpos += chSize / 2;
		pchString ++;
	}
}

void ssd1306_display_char(unsigned char chXpos, unsigned char chYpos, unsigned char chChr, unsigned char chSize, unsigned char chMode)
{      	
	unsigned char i, j;
	unsigned char chTemp, chYpos0 = chYpos;

	chChr = chChr - ' ';				   
	for (i = 0; i < chSize; i ++)
	{   
		if (chSize == 12) 
		{
			if (chMode) 
				chTemp = c_chFont1206[chChr][i];
			else 
				chTemp = ~c_chFont1206[chChr][i];
		} 
		else
		{
			if (chMode)
				chTemp = c_chFont1608[chChr][i];
			else 
				chTemp = ~c_chFont1608[chChr][i];
		}

		for (j = 0; j < 8; j ++) 
		{
			if (chTemp & 0x80)
				ssd1306_draw_point(chXpos, chYpos, 1);
			else 
				ssd1306_draw_point(chXpos, chYpos, 0);
			
			chTemp <<= 1;
			chYpos ++;

			if ((chYpos - chYpos0) == chSize) 
			{
				chYpos = chYpos0;
				chXpos ++;
				break;
			}
		}  	 
	} 
}

static void updateTime(uint8 gap) {
	printf("updataTime 100 \n");
	
    dataTimeStamp.time_ss += gap;
    
    /*
      날짜(년, 월, 일)는 변경하지 않도록 하자. 
      월마다 날수(28일, 29일, 30일, 31일)가 다르다. 
      윤년, 윤월 등등 그런것까지 모두 판별하기가 힘들다. 
      
      일단, 년, 월, 일은 사용하지 않는걸로 하고 시간만 계산하도록 하자. 
      */
    if( dataTimeStamp.time_ss >= 60 ) {
        dataTimeStamp.time_ss -= 60;
        
        dataTimeStamp.time_mm += 1;
        
        if( dataTimeStamp.time_mm >= 60 ) {
            dataTimeStamp.time_mm -= 60;
            
            dataTimeStamp.time_hh += 1;
            
            if( dataTimeStamp.time_hh >= 24 ) {
                dataTimeStamp.time_hh -= 24;                               
            }
        }
    }
	
	printf("current time : %d : %d : %d\n", dataTimeStamp.time_hh, dataTimeStamp.time_mm, dataTimeStamp.time_ss);
	
	sprintf(strCurruntTime, "%02d:%02d", dataTimeStamp.time_hh, dataTimeStamp.time_mm);
	
	printf("time : %s\n", strCurruntTime);
	
	// 초는 OLED에 표시가 안되니 계산하지 말자. 분단위까지만 나오는데, 보이지 않는 초가 변경되었다고 갱신하면 
	// 필요없는 갱신이 생긴다.
	dataTimeStamp.timeTotal = dataTimeStamp.time_hh + dataTimeStamp.time_mm; // + dataTimeStamp.time_ss; 
	
	printf("time total : %d, %d\n", dataTimeStamp.timeTotal, dataTimeStamp.timeTotalBackup );
	
	if( dataTimeStamp.timeTotalBackup != dataTimeStamp.timeTotal ) {
		printf("time modi\n");
		SHOW_CURRENT_TIME = true;		
		dataTimeStamp.timeTotalBackup = dataTimeStamp.timeTotal;		
	}		
	else {
		printf("time same\n");
	}
}

static void updateTimeForTest(uint8 gap) {
	printf("updataTime 100 \n");
	
    dataTimeStamp.time_ss += gap;
    
    /*
      날짜(년, 월, 일)는 변경하지 않도록 하자. 
      월마다 날수(28일, 29일, 30일, 31일)가 다르다. 
      윤년, 윤월 등등 그런것까지 모두 판별하기가 힘들다. 
      
      일단, 년, 월, 일은 사용하지 않는걸로 하고 시간만 계산하도록 하자. 
      */
    if( dataTimeStamp.time_ss >= 60 ) {
        dataTimeStamp.time_ss -= 60;
        
        dataTimeStamp.time_mm += 1;
        
        if( dataTimeStamp.time_mm >= 60 ) {
            dataTimeStamp.time_mm -= 60;
            
            dataTimeStamp.time_hh += 1;
            
            if( dataTimeStamp.time_hh >= 24 ) {
                dataTimeStamp.time_hh -= 24;                               
            }
        }
    }
	
	printf("current time : %d : %d : %d\n", dataTimeStamp.time_hh, dataTimeStamp.time_mm, dataTimeStamp.time_ss);
	
	sprintf(strCurruntTime, "%02d:%02d", dataTimeStamp.time_hh, dataTimeStamp.time_mm);
	
	printf("time : %s\n", strCurruntTime);
	
	// 초는 OLED에 표시가 안되니 계산하지 말자. 분단위까지만 나오는데, 보이지 않는 초가 변경되었다고 갱신하면 
	// 필요없는 갱신이 생긴다.
	dataTimeStamp.timeTotal = dataTimeStamp.time_hh + dataTimeStamp.time_mm; // + dataTimeStamp.time_ss; 
	
	printf("time total : %d, %d\n", dataTimeStamp.timeTotal, dataTimeStamp.timeTotalBackup );
	
	/*
	if( dataTimeStamp.timeTotalBackup != dataTimeStamp.timeTotal ) {
		printf("time modi\n");
		SHOW_CURRENT_TIME = true;		
		dataTimeStamp.timeTotalBackup = dataTimeStamp.timeTotal;		
	}		
	else {
		printf("time same\n");
	}
	*/
	SHOW_CURRENT_TIME = true;	
}

static void notiToAppFirmwareVersion() {
	uint32_t err_code;
	uint16 acca;
	uint8 checksum;
	
	printf("notiToAppFirmwareVersion start\n");
	
	memset(row_data, 0, 20);
	
	row_data[0] = 0xFF;
	row_data[1] = 0xDA;
	row_data[2] = 0x2;
	row_data[3] = FIRM_REV_MAJOR;
	row_data[4] = FIRM_REV_MINOR;

	getChecksum(row_data, row_data[2] + 3, &acca);
	checksum = LE8_L(acca);
    checksum = LE8_XOR(checksum);
	
	row_data[5] = checksum;
	
	err_code = ble_nus_string_send(&m_nus, row_data, 20);
	printf("EraseComplete: %u\n", err_code);	
	
	printf("notiToAppFirmwareVersion end\n");
}